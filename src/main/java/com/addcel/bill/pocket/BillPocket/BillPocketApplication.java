package com.addcel.bill.pocket.BillPocket;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.sql.DataSource;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class BillPocketApplication extends SpringBootServletInitializer {
	
	@Bean
	@Autowired
	DataSourceTransactionManager getDataSourceTransactionManager(DataSource datasource) {
		/* 30 */ return new DataSourceTransactionManager(datasource);
	}

	public static void main(String[] args) {
		/* 34 */ SpringApplication.run(BillPocketApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		/* 40 */ TrustStrategy acceptingTrustStrategy = (chain, authType) -> true;

		/* 44 */ SSLContext sslContext = SSLContexts.custom()
				.loadTrustMaterial(null, (TrustStrategy) acceptingTrustStrategy).build();

		/* 46 */ SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		/* 50 */ CloseableHttpClient httpClient = HttpClients.custom()
				.setSSLSocketFactory((LayeredConnectionSocketFactory) csf).build();

		/* 52 */ HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

		/* 55 */ requestFactory.setHttpClient((HttpClient) httpClient);

		/* 57 */ RestTemplate restTemplate = new RestTemplate((ClientHttpRequestFactory) requestFactory);
		/* 58 */ return restTemplate;
	}
}