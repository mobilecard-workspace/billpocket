package com.addcel.bill.pocket.BillPocket.antad;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consulta", propOrder = { "json" })
public class Consulta {
	protected String json;

	public String getJson() {
		/* 52 */ return this.json;
	}

	public void setJson(String value) {
		/* 64 */ this.json = value;
	}
}
