package com.addcel.bill.pocket.BillPocket.antad;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {
	/* 34 */ private static final QName _Consulta_QNAME = new QName("http://service.bridge.antad.addcel.com/",
			"consulta");
	/* 35 */ private static final QName _ConsultaResponse_QNAME = new QName("http://service.bridge.antad.addcel.com/",
			"consultaResponse");
	/* 36 */ private static final QName _ProcesaAutorizacion_QNAME = new QName(
			"http://service.bridge.antad.addcel.com/", "procesaAutorizacion");
	/* 37 */ private static final QName _ProcesaAutorizacionResponse_QNAME = new QName(
			"http://service.bridge.antad.addcel.com/", "procesaAutorizacionResponse");

	public Consulta createConsulta() {
		/* 51 */ return new Consulta();
	}

	public ConsultaResponse createConsultaResponse() {
		/* 59 */ return new ConsultaResponse();
	}

	public ProcesaAutorizacion createProcesaAutorizacion() {
		/* 67 */ return new ProcesaAutorizacion();
	}

	public ProcesaAutorizacionResponse createProcesaAutorizacionResponse() {
		/* 75 */ return new ProcesaAutorizacionResponse();
	}

	@XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "consulta")
	public JAXBElement<Consulta> createConsulta(Consulta value) {
		/* 84 */ return new JAXBElement<>(_Consulta_QNAME, Consulta.class, null, value);
	}

	@XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "consultaResponse")
	public JAXBElement<ConsultaResponse> createConsultaResponse(ConsultaResponse value) {
		/* 93 */ return new JAXBElement<>(_ConsultaResponse_QNAME, ConsultaResponse.class, null, value);
	}

	@XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "procesaAutorizacion")
	public JAXBElement<ProcesaAutorizacion> createProcesaAutorizacion(ProcesaAutorizacion value) {
		/* 102 */ return new JAXBElement<>(_ProcesaAutorizacion_QNAME, ProcesaAutorizacion.class, null, value);
	}

	@XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "procesaAutorizacionResponse")
	public JAXBElement<ProcesaAutorizacionResponse> createProcesaAutorizacionResponse(
			ProcesaAutorizacionResponse value) {
		/* 111 */ return new JAXBElement<>(_ProcesaAutorizacionResponse_QNAME, ProcesaAutorizacionResponse.class, null,
				value);
	}
}