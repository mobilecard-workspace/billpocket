package com.addcel.bill.pocket.BillPocket.antad;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "procesaAutorizacion", propOrder = { "json" })
public class ProcesaAutorizacion {
	protected String json;

	public String getJson() {
		/* 54 */ return this.json;
	}

	public void setJson(String value) {
		/* 66 */ this.json = value;
	}
}