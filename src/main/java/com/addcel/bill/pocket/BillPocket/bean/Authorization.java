package com.addcel.bill.pocket.BillPocket.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class Authorization extends BaseResponse {
	private String apiKey;
	private String cardToken;
	private double amount;
	private String txnType;
	private String contractNumber;
	private int paymentPlan;
	private String amexCustPostalCode;
	private String amexCustAddress;
	private String amexCustFirstName;
	private String amexCustLastName;
	private String amexCustEmailAddress;
	private String amexCustHostServerNm;
	private String amexCustBrowserTypDescTxt;
	private String amexShipToCtryCd;
	private String amexShipMthdCd;
	private String amexMerSKUNbr;
	private String amexCustIPAddr;
	private String amexCustIdPhoneNbr;
	private String amexCallTypId;
	private long idBitacora;
	private String pan;
	private String cvv2;
	private String expDate;
	private boolean amex;
	private long idTransaccion;

	public long getIdTransaccion() {
		/* 59 */ return this.idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		/* 63 */ this.idTransaccion = idTransaccion;
	}

	public String getApiKey() {
		/* 67 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 71 */ this.apiKey = apiKey;
	}

	public String getCardToken() {
		/* 75 */ return this.cardToken;
	}

	public void setCardToken(String cardToken) {
		/* 79 */ this.cardToken = cardToken;
	}

	public double getAmount() {
		/* 83 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 87 */ this.amount = amount;
	}

	public String getTxnType() {
		/* 91 */ return this.txnType;
	}

	public void setTxnType(String txnType) {
		/* 95 */ this.txnType = txnType;
	}

	public String getContractNumber() {
		/* 99 */ return this.contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		/* 103 */ this.contractNumber = contractNumber;
	}

	public int getPaymentPlan() {
		/* 107 */ return this.paymentPlan;
	}

	public void setPaymentPlan(int paymentPlan) {
		/* 111 */ this.paymentPlan = paymentPlan;
	}

	public String getAmexCustPostalCode() {
		/* 115 */ return this.amexCustPostalCode;
	}

	public void setAmexCustPostalCode(String amexCustPostalCode) {
		/* 119 */ this.amexCustPostalCode = amexCustPostalCode;
	}

	public String getAmexCustAddress() {
		/* 123 */ return this.amexCustAddress;
	}

	public void setAmexCustAddress(String amexCustAddress) {
		/* 127 */ this.amexCustAddress = amexCustAddress;
	}

	public String getAmexCustFirstName() {
		/* 131 */ return this.amexCustFirstName;
	}

	public void setAmexCustFirstName(String amexCustFirstName) {
		/* 135 */ this.amexCustFirstName = amexCustFirstName;
	}

	public String getAmexCustLastName() {
		/* 139 */ return this.amexCustLastName;
	}

	public void setAmexCustLastName(String amexCustLastName) {
		/* 143 */ this.amexCustLastName = amexCustLastName;
	}

	public String getAmexCustEmailAddress() {
		/* 147 */ return this.amexCustEmailAddress;
	}

	public void setAmexCustEmailAddress(String amexCustEmailAddress) {
		/* 151 */ this.amexCustEmailAddress = amexCustEmailAddress;
	}

	public String getAmexCustHostServerNm() {
		/* 155 */ return this.amexCustHostServerNm;
	}

	public void setAmexCustHostServerNm(String amexCustHostServerNm) {
		/* 159 */ this.amexCustHostServerNm = amexCustHostServerNm;
	}

	public String getAmexCustBrowserTypDescTxt() {
		/* 163 */ return this.amexCustBrowserTypDescTxt;
	}

	public void setAmexCustBrowserTypDescTxt(String amexCustBrowserTypDescTxt) {
		/* 167 */ this.amexCustBrowserTypDescTxt = amexCustBrowserTypDescTxt;
	}

	public String getAmexShipToCtryCd() {
		/* 171 */ return this.amexShipToCtryCd;
	}

	public void setAmexShipToCtryCd(String amexShipToCtryCd) {
		/* 175 */ this.amexShipToCtryCd = amexShipToCtryCd;
	}

	public String getAmexShipMthdCd() {
		/* 179 */ return this.amexShipMthdCd;
	}

	public void setAmexShipMthdCd(String amexShipMthdCd) {
		/* 183 */ this.amexShipMthdCd = amexShipMthdCd;
	}

	public String getAmexMerSKUNbr() {
		/* 187 */ return this.amexMerSKUNbr;
	}

	public void setAmexMerSKUNbr(String amexMerSKUNbr) {
		/* 191 */ this.amexMerSKUNbr = amexMerSKUNbr;
	}

	public String getAmexCustIPAddr() {
		/* 195 */ return this.amexCustIPAddr;
	}

	public void setAmexCustIPAddr(String amexCustIPAddr) {
		/* 199 */ this.amexCustIPAddr = amexCustIPAddr;
	}

	public String getAmexCustIdPhoneNbr() {
		/* 203 */ return this.amexCustIdPhoneNbr;
	}

	public void setAmexCustIdPhoneNbr(String amexCustIdPhoneNbr) {
		/* 207 */ this.amexCustIdPhoneNbr = amexCustIdPhoneNbr;
	}

	public String getAmexCallTypId() {
		/* 211 */ return this.amexCallTypId;
	}

	public void setAmexCallTypId(String amexCallTypId) {
		/* 215 */ this.amexCallTypId = amexCallTypId;
	}

	public long getIdBitacora() {
		/* 219 */ return this.idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		/* 223 */ this.idBitacora = idBitacora;
	}

	public String getPan() {
		/* 227 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 231 */ this.pan = pan;
	}

	public String getCvv2() {
		/* 235 */ return this.cvv2;
	}

	public void setCvv2(String cvv2) {
		/* 239 */ this.cvv2 = cvv2;
	}

	public String getExpDate() {
		/* 243 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 247 */ this.expDate = expDate;
	}

	public boolean isAmex() {
		/* 251 */ return this.amex;
	}

	public void setAmex(boolean amex) {
		/* 255 */ this.amex = amex;
	}
}