package com.addcel.bill.pocket.BillPocket.bean;

public class BillPocketTransaction {
	private String apiKey;
	private String cardToken;
	private double amount;
	private String txnType;
	private String contractNumber;
	private String concept;

	public String getApiKey() {
		/* 18 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 22 */ this.apiKey = apiKey;
	}

	public String getCardToken() {
		/* 26 */ return this.cardToken;
	}

	public void setCardToken(String cardToken) {
		/* 30 */ this.cardToken = cardToken;
	}

	public double getAmount() {
		/* 34 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 38 */ this.amount = amount;
	}

	public String getTxnType() {
		/* 42 */ return this.txnType;
	}

	public void setTxnType(String txnType) {
		/* 46 */ this.txnType = txnType;
	}

	public String getContractNumber() {
		/* 50 */ return this.contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		/* 54 */ this.contractNumber = contractNumber;
	}

	public String getConcept() {
		/* 58 */ return this.concept;
	}

	public void setConcept(String concept) {
		/* 62 */ this.concept = concept;
	}
}