package com.addcel.bill.pocket.BillPocket.bean;

public class CardDetail {
	private String pan;
	private String expDate;
	private String cvv2;

	public String getPan() {
		/* 12 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 16 */ this.pan = pan;
	}

	public String getExpDate() {
		/* 20 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 24 */ this.expDate = expDate;
	}

	public String getCvv2() {
		/* 28 */ return this.cvv2;
	}

	public void setCvv2(String cvv2) {
		/* 32 */ this.cvv2 = cvv2;
	}
}