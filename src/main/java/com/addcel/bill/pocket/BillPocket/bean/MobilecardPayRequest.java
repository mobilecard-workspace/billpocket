package com.addcel.bill.pocket.BillPocket.bean;

public class MobilecardPayRequest {
	private String concepto;
	private String referencia;
	private double cargo;
	private double comision;
	private String tipoTarjeta;
	private double lat;
	private double lon;
	private String pan;
	private String expDate;
	private String ct;

	public String getConcepto() {
		/* 26 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 30 */ this.concepto = concepto;
	}

	public String getReferencia() {
		/* 34 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 38 */ this.referencia = referencia;
	}

	public double getCargo() {
		/* 42 */ return this.cargo;
	}

	public void setCargo(double cargo) {
		/* 46 */ this.cargo = cargo;
	}

	public double getComision() {
		/* 50 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 54 */ this.comision = comision;
	}

	public String getTipoTarjeta() {
		/* 58 */ return this.tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		/* 62 */ this.tipoTarjeta = tipoTarjeta;
	}

	public double getLat() {
		/* 66 */ return this.lat;
	}

	public void setLat(double lat) {
		/* 70 */ this.lat = lat;
	}

	public double getLon() {
		/* 74 */ return this.lon;
	}

	public void setLon(double lon) {
		/* 78 */ this.lon = lon;
	}

	public String getPan() {
		/* 82 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 86 */ this.pan = pan;
	}

	public String getExpDate() {
		/* 90 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 94 */ this.expDate = expDate;
	}

	public String getCt() {
		/* 98 */ return this.ct;
	}

	public void setCt(String ct) {
		/* 102 */ this.ct = ct;
	}
}