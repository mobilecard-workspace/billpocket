package com.addcel.bill.pocket.BillPocket.bean;

public class Refund extends BaseResponse {
	private int opId;
	private String apiKey;

	public int getOpId() {
		/* 10 */ return this.opId;
	}

	public void setOpId(int opId) {
		/* 14 */ this.opId = opId;
	}

	public String getApiKey() {
		/* 18 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 22 */ this.apiKey = apiKey;
	}
}