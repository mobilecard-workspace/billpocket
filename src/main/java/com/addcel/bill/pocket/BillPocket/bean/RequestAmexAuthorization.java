package com.addcel.bill.pocket.BillPocket.bean;

public class RequestAmexAuthorization {
	private String apiKey;
	private String cardToken;
	private double amount;
	private String txnType;
	private String amexCustPostalCode;
	private String amexCustAddress;
	private String amexCustFirstName;
	private String amexCustLastName;
	private String amexCustEmailAddress;
	private String amexCustHostServerNm;
	private String amexCustBrowserTypDescTxt;
	private String amexShipToCtryCd;
	private String amexShipMthdCd;
	private String amexMerSKUNbr;
	private String amexCustIPAddr;
	private String amexCustIdPhoneNbr;
	private String amexCallTypId;

	public String getApiKey() {
		/* 41 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 45 */ this.apiKey = apiKey;
	}

	public String getCardToken() {
		/* 49 */ return this.cardToken;
	}

	public void setCardToken(String cardToken) {
		/* 53 */ this.cardToken = cardToken;
	}

	public double getAmount() {
		/* 57 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 61 */ this.amount = amount;
	}

	public String getTxnType() {
		/* 65 */ return this.txnType;
	}

	public void setTxnType(String txnType) {
		/* 69 */ this.txnType = txnType;
	}

	public String getAmexCustPostalCode() {
		/* 73 */ return this.amexCustPostalCode;
	}

	public void setAmexCustPostalCode(String amexCustPostalCode) {
		/* 77 */ this.amexCustPostalCode = amexCustPostalCode;
	}

	public String getAmexCustAddress() {
		/* 81 */ return this.amexCustAddress;
	}

	public void setAmexCustAddress(String amexCustAddress) {
		/* 85 */ this.amexCustAddress = amexCustAddress;
	}

	public String getAmexCustFirstName() {
		/* 89 */ return this.amexCustFirstName;
	}

	public void setAmexCustFirstName(String amexCustFirstName) {
		/* 93 */ this.amexCustFirstName = amexCustFirstName;
	}

	public String getAmexCustLastName() {
		/* 97 */ return this.amexCustLastName;
	}

	public void setAmexCustLastName(String amexCustLastName) {
		/* 101 */ this.amexCustLastName = amexCustLastName;
	}

	public String getAmexCustEmailAddress() {
		/* 105 */ return this.amexCustEmailAddress;
	}

	public void setAmexCustEmailAddress(String amexCustEmailAddress) {
		/* 109 */ this.amexCustEmailAddress = amexCustEmailAddress;
	}

	public String getAmexCustHostServerNm() {
		/* 113 */ return this.amexCustHostServerNm;
	}

	public void setAmexCustHostServerNm(String amexCustHostServerNm) {
		/* 117 */ this.amexCustHostServerNm = amexCustHostServerNm;
	}

	public String getAmexCustBrowserTypDescTxt() {
		/* 121 */ return this.amexCustBrowserTypDescTxt;
	}

	public void setAmexCustBrowserTypDescTxt(String amexCustBrowserTypDescTxt) {
		/* 125 */ this.amexCustBrowserTypDescTxt = amexCustBrowserTypDescTxt;
	}

	public String getAmexShipToCtryCd() {
		/* 129 */ return this.amexShipToCtryCd;
	}

	public void setAmexShipToCtryCd(String amexShipToCtryCd) {
		/* 133 */ this.amexShipToCtryCd = amexShipToCtryCd;
	}

	public String getAmexShipMthdCd() {
		/* 137 */ return this.amexShipMthdCd;
	}

	public void setAmexShipMthdCd(String amexShipMthdCd) {
		/* 141 */ this.amexShipMthdCd = amexShipMthdCd;
	}

	public String getAmexMerSKUNbr() {
		/* 145 */ return this.amexMerSKUNbr;
	}

	public void setAmexMerSKUNbr(String amexMerSKUNbr) {
		/* 149 */ this.amexMerSKUNbr = amexMerSKUNbr;
	}

	public String getAmexCustIPAddr() {
		/* 153 */ return this.amexCustIPAddr;
	}

	public void setAmexCustIPAddr(String amexCustIPAddr) {
		/* 157 */ this.amexCustIPAddr = amexCustIPAddr;
	}

	public String getAmexCustIdPhoneNbr() {
		/* 161 */ return this.amexCustIdPhoneNbr;
	}

	public void setAmexCustIdPhoneNbr(String amexCustIdPhoneNbr) {
		/* 165 */ this.amexCustIdPhoneNbr = amexCustIdPhoneNbr;
	}

	public String getAmexCallTypId() {
		/* 169 */ return this.amexCallTypId;
	}

	public void setAmexCallTypId(String amexCallTypId) {
		/* 173 */ this.amexCallTypId = amexCallTypId;
	}
}