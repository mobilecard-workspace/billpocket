package com.addcel.bill.pocket.BillPocket.bean;

public class RequestAuthorization {
	private String apiKey;
	private String cardToken;
	private double amount;
	private String txnType;

	public String getApiKey() {
		/* 14 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 18 */ this.apiKey = apiKey;
	}

	public String getCardToken() {
		/* 22 */ return this.cardToken;
	}

	public void setCardToken(String cardToken) {
		/* 26 */ this.cardToken = cardToken;
	}

	public double getAmount() {
		/* 30 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 34 */ this.amount = amount;
	}

	public String getTxnType() {
		/* 38 */ return this.txnType;
	}

	public void setTxnType(String txnType) {
		/* 42 */ this.txnType = txnType;
	}
}