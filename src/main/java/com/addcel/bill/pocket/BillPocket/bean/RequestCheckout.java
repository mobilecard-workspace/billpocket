package com.addcel.bill.pocket.BillPocket.bean;

public class RequestCheckout {
	private String apiKey;
	private String externalId;
	private String[] items;
	private double total;

	public String getApiKey() {
		/* 14 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 18 */ this.apiKey = apiKey;
	}

	public String getExternalId() {
		/* 22 */ return this.externalId;
	}

	public void setExternalId(String externalId) {
		/* 26 */ this.externalId = externalId;
	}

	public String[] getItems() {
		/* 30 */ return this.items;
	}

	public void setItems(String[] items) {
		/* 34 */ this.items = items;
	}

	public double getTotal() {
		/* 38 */ return this.total;
	}

	public void setTotal(double total) {
		/* 42 */ this.total = total;
	}
}