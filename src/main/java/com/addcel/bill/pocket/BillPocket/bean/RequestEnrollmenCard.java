package com.addcel.bill.pocket.BillPocket.bean;

public class RequestEnrollmenCard {
	private String pan;
	private String cvv2;
	private String expDate;
	private String apiKey;

	public String getPan() {
		/* 14 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 18 */ this.pan = pan;
	}

	public String getCvv2() {
		/* 22 */ return this.cvv2;
	}

	public void setCvv2(String cvv2) {
		/* 26 */ this.cvv2 = cvv2;
	}

	public String getExpDate() {
		/* 30 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 34 */ this.expDate = expDate;
	}

	public String getApiKey() {
		/* 38 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 42 */ this.apiKey = apiKey;
	}
}