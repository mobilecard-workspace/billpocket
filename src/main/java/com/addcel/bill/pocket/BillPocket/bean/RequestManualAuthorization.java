package com.addcel.bill.pocket.BillPocket.bean;

public class RequestManualAuthorization {
	private String apiKey;
	private String cardToken;
	private double amount;
	private String txnType;
	private CardDetail cardReq;

	public String getApiKey() {
		/* 16 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 20 */ this.apiKey = apiKey;
	}

	public String getCardToken() {
		/* 24 */ return this.cardToken;
	}

	public void setCardToken(String cardToken) {
		/* 28 */ this.cardToken = cardToken;
	}

	public double getAmount() {
		/* 32 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 36 */ this.amount = amount;
	}

	public String getTxnType() {
		/* 40 */ return this.txnType;
	}

	public void setTxnType(String txnType) {
		/* 44 */ this.txnType = txnType;
	}

	public CardDetail getCardReq() {
		/* 48 */ return this.cardReq;
	}

	public void setCardReq(CardDetail cardReq) {
		/* 52 */ this.cardReq = cardReq;
	}
}