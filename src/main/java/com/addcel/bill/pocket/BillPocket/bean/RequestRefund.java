package com.addcel.bill.pocket.BillPocket.bean;

public class RequestRefund {
	private Integer opId;
	private String apiKey;

	public Integer getOpId() {
		/* 10 */ return this.opId;
	}

	public void setOpId(Integer opId) {
		/* 14 */ this.opId = opId;
	}

	public String getApiKey() {
		/* 18 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 22 */ this.apiKey = apiKey;
	}
}