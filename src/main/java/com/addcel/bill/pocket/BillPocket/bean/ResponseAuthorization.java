package com.addcel.bill.pocket.BillPocket.bean;

public class ResponseAuthorization extends BaseResponse {
	private Integer status;
	private Integer opId;
	private String txnISOCode;
	private String authNumber;
	private String ticketUrl;
	private double amount;
	private String maskedPAN;
	private long dateTime;
	private long idTransaccion;
	private String referencia;

	public Integer getStatus() {
		/* 26 */ return this.status;
	}

	public void setStatus(Integer status) {
		/* 30 */ this.status = status;
	}

	public Integer getOpId() {
		/* 34 */ return this.opId;
	}

	public void setOpId(Integer opId) {
		/* 38 */ this.opId = opId;
	}

	public String getTxnISOCode() {
		/* 42 */ return this.txnISOCode;
	}

	public void setTxnISOCode(String txnISOCode) {
		/* 46 */ this.txnISOCode = txnISOCode;
	}

	public String getAuthNumber() {
		/* 50 */ return this.authNumber;
	}

	public void setAuthNumber(String authNumber) {
		/* 54 */ this.authNumber = authNumber;
	}

	public String getTicketUrl() {
		/* 58 */ return this.ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		/* 62 */ this.ticketUrl = ticketUrl;
	}

	public double getAmount() {
		/* 66 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 70 */ this.amount = amount;
	}

	public String getMaskedPAN() {
		/* 74 */ return this.maskedPAN;
	}

	public void setMaskedPAN(String maskedPAN) {
		/* 78 */ this.maskedPAN = maskedPAN;
	}

	public long getDateTime() {
		/* 82 */ return this.dateTime;
	}

	public void setDateTime(long dateTime) {
		/* 86 */ this.dateTime = dateTime;
	}

	public long getIdTransaccion() {
		/* 90 */ return this.idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		/* 94 */ this.idTransaccion = idTransaccion;
	}

	public String getReferencia() {
		/* 98 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 102 */ this.referencia = referencia;
	}
}