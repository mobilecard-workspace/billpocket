package com.addcel.bill.pocket.BillPocket.bean;

public class ResponseCardTokenization {
	private Integer status;
	private String message;
	private String cardToken;
	private String maskedPan;

	public Integer getStatus() {
		/* 14 */ return this.status;
	}

	public void setStatus(Integer status) {
		/* 18 */ this.status = status;
	}

	public String getMessage() {
		/* 22 */ return this.message;
	}

	public void setMessage(String message) {
		/* 26 */ this.message = message;
	}

	public String getCardToken() {
		/* 30 */ return this.cardToken;
	}

	public void setCardToken(String cardToken) {
		/* 34 */ this.cardToken = cardToken;
	}

	public String getMaskedPan() {
		/* 38 */ return this.maskedPan;
	}

	public void setMaskedPan(String maskedPan) {
		/* 42 */ this.maskedPan = maskedPan;
	}
}