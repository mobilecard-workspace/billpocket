package com.addcel.bill.pocket.BillPocket.bean;

public class ResponseCheckout extends BaseResponse {
	private String[] items;
	private String checkoutId;
	private String externalId;
	private double total;
	private String createdAt;
	private String updatedAt;

	public String[] getItems() {
		/* 19 */ return this.items;
	}

	public void setItems(String[] items) {
		/* 23 */ this.items = items;
	}

	public String getCheckoutId() {
		/* 27 */ return this.checkoutId;
	}

	public void setCheckoutId(String checkoutId) {
		/* 31 */ this.checkoutId = checkoutId;
	}

	public String getExternalId() {
		/* 35 */ return this.externalId;
	}

	public void setExternalId(String externalId) {
		/* 39 */ this.externalId = externalId;
	}

	public double getTotal() {
		/* 43 */ return this.total;
	}

	public void setTotal(double total) {
		/* 47 */ this.total = total;
	}

	public String getCreatedAt() {
		/* 51 */ return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		/* 55 */ this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		/* 59 */ return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		/* 63 */ this.updatedAt = updatedAt;
	}
}