package com.addcel.bill.pocket.BillPocket.bean;

public class ResponseEnrollmentCard {
	private int code;
	private String message;
	private String cardToken;
	private int status;
	private String maskedPan;

	public int getCode() {
		/* 17 */ return this.code;
	}

	public void setCode(int code) {
		/* 21 */ this.code = code;
	}

	public String getMessage() {
		/* 25 */ return this.message;
	}

	public void setMessage(String message) {
		/* 29 */ this.message = message;
	}

	public String getCardToken() {
		/* 33 */ return this.cardToken;
	}

	public void setCardToken(String cardToken) {
		/* 37 */ this.cardToken = cardToken;
	}

	public int getStatus() {
		/* 41 */ return this.status;
	}

	public void setStatus(int status) {
		/* 45 */ this.status = status;
	}

	public String getMaskedPan() {
		/* 49 */ return this.maskedPan;
	}

	public void setMaskedPan(String maskedPan) {
		/* 53 */ this.maskedPan = maskedPan;
	}
}