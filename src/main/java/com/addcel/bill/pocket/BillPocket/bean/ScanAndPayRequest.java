package com.addcel.bill.pocket.BillPocket.bean;

public class ScanAndPayRequest {
	private long idEstablecimiento;
	private Double monto;
	private Double propina;
	private Double comision;
	private long idBitacora;
	private String concepto;
	private int status;
	private String emailCliente;

	public long getIdEstablecimiento() {
		/* 15 */ return this.idEstablecimiento;
	}

	public void setIdEstablecimiento(long idEstablecimiento) {
		/* 19 */ this.idEstablecimiento = idEstablecimiento;
	}

	public Double getMonto() {
		/* 23 */ return this.monto;
	}

	public void setMonto(Double monto) {
		/* 27 */ this.monto = monto;
	}

	public Double getPropina() {
		/* 31 */ return this.propina;
	}

	public void setPropina(Double propina) {
		/* 35 */ this.propina = propina;
	}

	public Double getComision() {
		/* 39 */ return this.comision;
	}

	public void setComision(Double comision) {
		/* 43 */ this.comision = comision;
	}

	public long getIdBitacora() {
		/* 47 */ return this.idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		/* 51 */ this.idBitacora = idBitacora;
	}

	public String getConcepto() {
		/* 55 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 59 */ this.concepto = concepto;
	}

	public int getStatus() {
		/* 63 */ return this.status;
	}

	public void setStatus(int status) {
		/* 67 */ this.status = status;
	}

	public String getEmailCliente() {
		/* 71 */ return this.emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		/* 75 */ this.emailCliente = emailCliente;
	}
}