package com.addcel.bill.pocket.BillPocket.bean;

public class TransactionManualRequest {
	private long idUsuario;
	private long idEstablecimiento;
	private String idioma;
	private int idProveedor;
	private int idProducto;
	private String concepto;
	private String referencia;
	private double cargo;
	private double comision;
	private String tipoTarjeta;
	private String imei;
	private String software;
	private String modelo;
	private double lat;
	private double lon;
	private String operacion;
	private String emisor;
	private String pan;
	private String expDate;
	private String ct;
	private double propina;
	private int idTarjeta;
	private long idBitacora;

	public long getIdUsuario() {
		/* 52 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 56 */ this.idUsuario = idUsuario;
	}

	public String getIdioma() {
		/* 60 */ return this.idioma;
	}

	public void setIdioma(String idioma) {
		/* 64 */ this.idioma = idioma;
	}

	public int getIdProveedor() {
		/* 68 */ return this.idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		/* 72 */ this.idProveedor = idProveedor;
	}

	public int getIdProducto() {
		/* 76 */ return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		/* 80 */ this.idProducto = idProducto;
	}

	public String getConcepto() {
		/* 84 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 88 */ this.concepto = concepto;
	}

	public String getReferencia() {
		/* 92 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 96 */ this.referencia = referencia;
	}

	public double getCargo() {
		/* 100 */ return this.cargo;
	}

	public void setCargo(double cargo) {
		/* 104 */ this.cargo = cargo;
	}

	public double getComision() {
		/* 108 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 112 */ this.comision = comision;
	}

	public String getTipoTarjeta() {
		/* 116 */ return this.tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		/* 120 */ this.tipoTarjeta = tipoTarjeta;
	}

	public String getImei() {
		/* 124 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 128 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 132 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 136 */ this.software = software;
	}

	public String getModelo() {
		/* 140 */ return this.modelo;
	}

	public void setModelo(String modelo) {
		/* 144 */ this.modelo = modelo;
	}

	public double getLat() {
		/* 148 */ return this.lat;
	}

	public void setLat(double lat) {
		/* 152 */ this.lat = lat;
	}

	public double getLon() {
		/* 156 */ return this.lon;
	}

	public void setLon(double lon) {
		/* 160 */ this.lon = lon;
	}

	public String getOperacion() {
		/* 164 */ return this.operacion;
	}

	public void setOperacion(String operacion) {
		/* 168 */ this.operacion = operacion;
	}

	public String getEmisor() {
		/* 172 */ return this.emisor;
	}

	public void setEmisor(String emisor) {
		/* 176 */ this.emisor = emisor;
	}

	public String getPan() {
		/* 180 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 184 */ this.pan = pan;
	}

	public String getExpDate() {
		/* 188 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 192 */ this.expDate = expDate;
	}

	public String getCt() {
		/* 196 */ return this.ct;
	}

	public void setCt(String ct) {
		/* 200 */ this.ct = ct;
	}

	public long getIdEstablecimiento() {
		/* 204 */ return this.idEstablecimiento;
	}

	public void setIdEstablecimiento(long idEstablecimiento) {
		/* 208 */ this.idEstablecimiento = idEstablecimiento;
	}

	public double getPropina() {
		/* 212 */ return this.propina;
	}

	public void setPropina(double propina) {
		/* 216 */ this.propina = propina;
	}

	public int getIdTarjeta() {
		/* 220 */ return this.idTarjeta;
	}

	public void setIdTarjeta(int idTarjeta) {
		/* 224 */ this.idTarjeta = idTarjeta;
	}

	public long getIdBitacora() {
		/* 228 */ return this.idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		/* 232 */ this.idBitacora = idBitacora;
	}
}