package com.addcel.bill.pocket.BillPocket.bean;

public class TransactionRequest {
	private long idUsuario;
	private String idioma;
	private int idProveedor;
	private int idProducto;
	private String concepto;
	private String referencia;
	private double cargo;
	private double comision;
	private int idTarjeta;
	private String tipoTarjeta;
	private String imei;
	private String software;
	private String modelo;
	private double lat;
	private double lon;
	private String operacion;
	private String emisor;

	public long getIdUsuario() {
		/* 40 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 44 */ this.idUsuario = idUsuario;
	}

	public String getIdioma() {
		/* 48 */ return this.idioma;
	}

	public void setIdioma(String idioma) {
		/* 52 */ this.idioma = idioma;
	}

	public int getIdProveedor() {
		/* 56 */ return this.idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		/* 60 */ this.idProveedor = idProveedor;
	}

	public int getIdProducto() {
		/* 64 */ return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		/* 68 */ this.idProducto = idProducto;
	}

	public String getConcepto() {
		/* 72 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 76 */ this.concepto = concepto;
	}

	public String getReferencia() {
		/* 80 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 84 */ this.referencia = referencia;
	}

	public double getCargo() {
		/* 88 */ return this.cargo;
	}

	public void setCargo(double cargo) {
		/* 92 */ this.cargo = cargo;
	}

	public double getComision() {
		/* 96 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 100 */ this.comision = comision;
	}

	public int getIdTarjeta() {
		/* 104 */ return this.idTarjeta;
	}

	public void setIdTarjeta(int idTarjeta) {
		/* 108 */ this.idTarjeta = idTarjeta;
	}

	public String getTipoTarjeta() {
		/* 112 */ return this.tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		/* 116 */ this.tipoTarjeta = tipoTarjeta;
	}

	public String getImei() {
		/* 120 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 124 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 128 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 132 */ this.software = software;
	}

	public String getModelo() {
		/* 136 */ return this.modelo;
	}

	public void setModelo(String modelo) {
		/* 140 */ this.modelo = modelo;
	}

	public double getLat() {
		/* 144 */ return this.lat;
	}

	public void setLat(double lat) {
		/* 148 */ this.lat = lat;
	}

	public double getLon() {
		/* 152 */ return this.lon;
	}

	public void setLon(double lon) {
		/* 156 */ this.lon = lon;
	}

	public String getOperacion() {
		/* 160 */ return this.operacion;
	}

	public void setOperacion(String operacion) {
		/* 164 */ this.operacion = operacion;
	}

	public String getEmisor() {
		/* 168 */ return this.emisor;
	}

	public void setEmisor(String emisor) {
		/* 172 */ this.emisor = emisor;
	}
}