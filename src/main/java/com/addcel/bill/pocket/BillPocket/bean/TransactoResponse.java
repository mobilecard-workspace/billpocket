package com.addcel.bill.pocket.BillPocket.bean;

public class TransactoResponse {
	private int idError;
	private String mensajeError;

	public int getIdError() {
		/* 10 */ return this.idError;
	}

	public void setIdError(int idError) {
		/* 14 */ this.idError = idError;
	}

	public String getMensajeError() {
		/* 18 */ return this.mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		/* 22 */ this.mensajeError = mensajeError;
	}
}