package com.addcel.bill.pocket.BillPocket.bean;

public class TransferRequest {
	private long idBitacora;
	private long idUser;
	private int idCard;
	private String accountId;
	private double amount;
	private String concept;
	private double comision;
	private String imei;
	private String software;
	private String model;
	private double lat;
	private double lon;
	private String referencia;
	private String emisor;
	private String operation;
	private int status;

	public long getIdUser() {
		/* 41 */ return this.idUser;
	}

	public void setIdUser(long idUser) {
		/* 45 */ this.idUser = idUser;
	}

	public int getIdCard() {
		/* 49 */ return this.idCard;
	}

	public void setIdCard(int idCard) {
		/* 53 */ this.idCard = idCard;
	}

	public String getAccountId() {
		/* 57 */ return this.accountId;
	}

	public void setAccountId(String accountId) {
		/* 61 */ this.accountId = accountId;
	}

	public double getAmount() {
		/* 65 */ return this.amount;
	}

	public void setAmount(double amount) {
		/* 69 */ this.amount = amount;
	}

	public String getConcept() {
		/* 73 */ return this.concept;
	}

	public void setConcept(String concept) {
		/* 77 */ this.concept = concept;
	}

	public double getComision() {
		/* 81 */ return this.comision;
	}

	public void setComision(double comision) {
		/* 85 */ this.comision = comision;
	}

	public String getImei() {
		/* 89 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 93 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 97 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 101 */ this.software = software;
	}

	public String getModel() {
		/* 105 */ return this.model;
	}

	public void setModel(String model) {
		/* 109 */ this.model = model;
	}

	public double getLat() {
		/* 113 */ return this.lat;
	}

	public void setLat(double lat) {
		/* 117 */ this.lat = lat;
	}

	public double getLon() {
		/* 121 */ return this.lon;
	}

	public void setLon(double lon) {
		/* 125 */ this.lon = lon;
	}

	public String getReferencia() {
		/* 129 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 133 */ this.referencia = referencia;
	}

	public String getEmisor() {
		/* 137 */ return this.emisor;
	}

	public void setEmisor(String emisor) {
		/* 141 */ this.emisor = emisor;
	}

	public String getOperation() {
		/* 145 */ return this.operation;
	}

	public void setOperation(String operation) {
		/* 149 */ this.operation = operation;
	}

	public long getIdBitacora() {
		/* 153 */ return this.idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		/* 157 */ this.idBitacora = idBitacora;
	}

	public int getStatus() {
		/* 161 */ return this.status;
	}

	public void setStatus(int status) {
		/* 165 */ this.status = status;
	}
}