package com.addcel.bill.pocket.BillPocket.bean;

public class TreeDResponse {
	private String checkout;
	private String externalId;
	private String total;
	private String authorization;
	private String ticketUrl;
	private String opId;
	private String maskedPAN;
	private String message;
	private String token;
	private String signature;

	public String getCheckout() {
		/* 26 */ return this.checkout;
	}

	public void setCheckout(String checkout) {
		/* 30 */ this.checkout = checkout;
	}

	public String getExternalId() {
		/* 34 */ return this.externalId;
	}

	public void setExternalId(String externalId) {
		/* 38 */ this.externalId = externalId;
	}

	public String getTotal() {
		/* 42 */ return this.total;
	}

	public void setTotal(String total) {
		/* 46 */ this.total = total;
	}

	public String getAuthorization() {
		/* 50 */ return this.authorization;
	}

	public void setAuthorization(String authorization) {
		/* 54 */ this.authorization = authorization;
	}

	public String getTicketUrl() {
		/* 58 */ return this.ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		/* 62 */ this.ticketUrl = ticketUrl;
	}

	public String getOpId() {
		/* 66 */ return this.opId;
	}

	public void setOpId(String opId) {
		/* 70 */ this.opId = opId;
	}

	public String getMaskedPAN() {
		/* 74 */ return this.maskedPAN;
	}

	public void setMaskedPAN(String maskedPAN) {
		/* 78 */ this.maskedPAN = maskedPAN;
	}

	public String getMessage() {
		/* 82 */ return this.message;
	}

	public void setMessage(String message) {
		/* 86 */ this.message = message;
	}

	public String getToken() {
		/* 90 */ return this.token;
	}

	public void setToken(String token) {
		/* 94 */ this.token = token;
	}

	public String getSignature() {
		/* 98 */ return this.signature;
	}

	public void setSignature(String signature) {
		/* 102 */ this.signature = signature;
	}
}