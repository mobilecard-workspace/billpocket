package com.addcel.bill.pocket.BillPocket.bean;

public class Webhook {
	private String checkout;
	private String externalId;
	private String total;
	private String authorization;
	private String ticketUrl;
	private String opId;
	private String maskedPAN;
	private String message;
	private String token;
	private String signature;

	public String getCheckout() {
		/* 28 */ return this.checkout;
	}

	public void setCheckout(String checkout) {
		/* 32 */ this.checkout = checkout;
	}

	public String getExternalId() {
		/* 36 */ return this.externalId;
	}

	public void setExternalId(String externalId) {
		/* 40 */ this.externalId = externalId;
	}

	public String getTotal() {
		/* 44 */ return this.total;
	}

	public void setTotal(String total) {
		/* 48 */ this.total = total;
	}

	public String getAuthorization() {
		/* 52 */ return this.authorization;
	}

	public void setAuthorization(String authorization) {
		/* 56 */ this.authorization = authorization;
	}

	public String getTicketUrl() {
		/* 60 */ return this.ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		/* 64 */ this.ticketUrl = ticketUrl;
	}

	public String getOpId() {
		/* 68 */ return this.opId;
	}

	public void setOpId(String opId) {
		/* 72 */ this.opId = opId;
	}

	public String getMaskedPAN() {
		/* 76 */ return this.maskedPAN;
	}

	public void setMaskedPAN(String maskedPAN) {
		/* 80 */ this.maskedPAN = maskedPAN;
	}

	public String getMessage() {
		/* 84 */ return this.message;
	}

	public void setMessage(String message) {
		/* 88 */ this.message = message;
	}

	public String getToken() {
		/* 92 */ return this.token;
	}

	public void setToken(String token) {
		/* 96 */ this.token = token;
	}

	public String getSignature() {
		/* 100 */ return this.signature;
	}

	public void setSignature(String signature) {
		/* 104 */ this.signature = signature;
	}
}