package com.addcel.bill.pocket.BillPocket.client;

public class PushParams {
	private String name;
	private String value;

	public String getName() {
		/* 11 */ return this.name;
	}

	public void setName(String name) {
		/* 15 */ this.name = name;
	}

	public String getValue() {
		/* 19 */ return this.value;
	}

	public void setValue(String value) {
		/* 23 */ this.value = value;
	}
}