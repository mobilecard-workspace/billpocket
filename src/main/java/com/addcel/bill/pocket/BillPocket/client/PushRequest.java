package com.addcel.bill.pocket.BillPocket.client;

import java.util.List;

public class PushRequest {
	private long modulo;
	private long id_usuario;
	private String tipoUsuario;
	private String idioma;
	private int idPais;
	private int idApp;
	private List<PushParams> params;

	public long getModulo() {
		/* 16 */ return this.modulo;
	}

	public void setModulo(long modulo) {
		/* 20 */ this.modulo = modulo;
	}

	public long getId_usuario() {
		/* 24 */ return this.id_usuario;
	}

	public void setId_usuario(long id_usuario) {
		/* 28 */ this.id_usuario = id_usuario;
	}

	public String getTipoUsuario() {
		/* 32 */ return this.tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		/* 36 */ this.tipoUsuario = tipoUsuario;
	}

	public String getIdioma() {
		/* 40 */ return this.idioma;
	}

	public void setIdioma(String idioma) {
		/* 44 */ this.idioma = idioma;
	}

	public int getIdPais() {
		/* 48 */ return this.idPais;
	}

	public void setIdPais(int idPais) {
		/* 52 */ this.idPais = idPais;
	}

	public int getIdApp() {
		/* 56 */ return this.idApp;
	}

	public void setIdApp(int idApp) {
		/* 60 */ this.idApp = idApp;
	}

	public List<PushParams> getParams() {
		/* 64 */ return this.params;
	}

	public void setParams(List<PushParams> params) {
		/* 68 */ this.params = params;
	}
}