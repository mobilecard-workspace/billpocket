package com.addcel.bill.pocket.BillPocket.client;

public class PushResponse {
	private int code;
	private String message;

	public int getCode() {
		/* 10 */ return this.code;
	}

	public void setCode(int code) {
		/* 14 */ this.code = code;
	}

	public String getMessage() {
		/* 18 */ return this.message;
	}

	public void setMessage(String message) {
		/* 22 */ this.message = message;
	}
}