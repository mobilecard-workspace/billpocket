package com.addcel.bill.pocket.BillPocket.controller;

import com.addcel.bill.pocket.BillPocket.bean.BaseResponse;
import com.addcel.bill.pocket.BillPocket.bean.BillPocketTransaction;
import com.addcel.bill.pocket.BillPocket.bean.MobilecardPayRequest;
import com.addcel.bill.pocket.BillPocket.bean.TransactionManualRequest;
import com.addcel.bill.pocket.BillPocket.bean.TransactionRequest;
import com.addcel.bill.pocket.BillPocket.service.BillPocketClientService;
import com.addcel.bill.pocket.BillPocket.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BillPocketController {
	@Autowired
	private PaymentService paymentService;
	@Autowired
	private BillPocketClientService billPocketService;
	private static final String PATH_ENROLL_CARD = "/{idApp}/{idPais}/{idioma}/enroll/card";
	private static final String PATH_AUTHORIZATION = "/{idApp}/{idPais}/{idioma}/authorization";
	private static final String PATH_REFUND = "/{idApp}/{idPais}/{idioma}/refund";
	private static final String PATH_CHECK_TRANSACTION = "/{idApp}/{idPais}/{idioma}/check";
	private static final String PATH_AUTHORIZATION_MANUAL = "/{idApp}/{idPais}/{idioma}/authorization/manual";
	private static final String PATH_AUTHORIZATION_SCAN = "/{idApp}/{idPais}/{idioma}/authorization/scan";
	private static final String PATH_AUTHORIZATION_BY_ID = "/{idApp}/{idPais}/{idioma}/authorization/id";
	private static final String PATH_AUTHORIZATION_MOBILECARD = "/{idApp}/{idPais}/{idioma}/mobilecard/pay";

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/mobilecard/pay" }, method = {
			RequestMethod.POST }, produces = { "application/json;charset=UTF-8" })
	public ResponseEntity<Object> verifyEnrollmentCardholder(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestBody MobilecardPayRequest transaction) {
		/* 44 */ ResponseEntity<Object> response = null;
		try {
			/* 46 */ response = new ResponseEntity(
					this.paymentService.mobilecardPayRequest(idApp, idPais, idioma, transaction), HttpStatus.OK);
		}
		/* 48 */ catch (Exception e) {
			/* 49 */ e.printStackTrace();
			/* 50 */ BaseResponse error = new BaseResponse();
			/* 51 */ error.setCode(-10);
			/* 52 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 53 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 55 */ return response;
	}

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/enroll/card" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	public ResponseEntity<Object> verifyEnrollmentCardholder(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestParam int idTarjeta, @RequestParam long idUsuario,
			@RequestBody BillPocketTransaction transaction) {
		/* 65 */ ResponseEntity<Object> response = null;
		try {
			/* 67 */ response = new ResponseEntity(
					this.paymentService.cardTokenization(idApp, idPais, idioma, idUsuario, Integer.valueOf(idTarjeta)),
					HttpStatus.OK);
		}
		/* 69 */ catch (Exception e) {
			/* 70 */ e.printStackTrace();
			/* 71 */ BaseResponse error = new BaseResponse();
			/* 72 */ error.setCode(-10);
			/* 73 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 74 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 76 */ return response;
	}

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/authorization" }, method = {
			RequestMethod.POST }, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> authorizationRequest(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestBody TransactionRequest transaction) {
		/* 85 */ ResponseEntity<Object> response = null;
		try {
			/* 87 */ response = new ResponseEntity(
					this.paymentService.authorization(idApp, idPais, idioma, transaction), HttpStatus.OK);
		}
		/* 89 */ catch (Exception e) {
			/* 90 */ e.printStackTrace();
			/* 91 */ BaseResponse error = new BaseResponse();
			/* 92 */ error.setCode(-10);
			/* 93 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 94 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 96 */ return response;
	}

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/authorization/manual" }, method = {
			RequestMethod.POST }, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> authorizationManualRequest(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestBody TransactionManualRequest transaction) {
		/* 106 */ ResponseEntity<Object> response = null;
		try {
			/* 108 */ response = new ResponseEntity(
					this.paymentService.authorizationManualRequest(idApp, idPais, idioma, transaction), HttpStatus.OK);
		}
		/* 110 */ catch (Exception e) {
			/* 111 */ e.printStackTrace();
			/* 112 */ BaseResponse error = new BaseResponse();
			/* 113 */ error.setCode(-10);
			/* 114 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 115 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 117 */ return response;
	}

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/authorization/id" }, method = {
			RequestMethod.POST }, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> authorizationById(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestBody TransactionManualRequest transaction) {
		/* 126 */ ResponseEntity<Object> response = null;
		try {
			/* 128 */ response = new ResponseEntity(
					this.paymentService.authorizationById(idApp, idPais, idioma, transaction), HttpStatus.OK);
		}
		/* 130 */ catch (Exception e) {
			/* 131 */ e.printStackTrace();
			/* 132 */ BaseResponse error = new BaseResponse();
			/* 133 */ error.setCode(-10);
			/* 134 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 135 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 137 */ return response;
	}

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/authorization/scan" }, method = {
			RequestMethod.POST }, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> authorizationByUserScanRequest(@PathVariable Integer idApp,
			@PathVariable Integer idPais, @PathVariable String idioma, @RequestBody TransactionRequest transaction) {
		/* 146 */ ResponseEntity<Object> response = null;
		try {
			/* 148 */ response = new ResponseEntity(
					this.paymentService.authorizationByUserScanRequest(idApp, idPais, idioma, transaction),
					HttpStatus.OK);
		}
		/* 150 */ catch (Exception e) {
			/* 151 */ e.printStackTrace();
			/* 152 */ BaseResponse error = new BaseResponse();
			/* 153 */ error.setCode(-10);
			/* 154 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 155 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 157 */ return response;
	}

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/refund" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> refundRequest(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestParam Integer idTransaccion) {
		/* 166 */ ResponseEntity<Object> response = null;
		try {
			/* 168 */ response = new ResponseEntity(
					this.paymentService.refund(idApp, idPais, idioma, idTransaccion.intValue()), HttpStatus.OK);
		}
		/* 170 */ catch (Exception e) {
			/* 171 */ e.printStackTrace();
			/* 172 */ BaseResponse error = new BaseResponse();
			/* 173 */ error.setCode(-10);
			/* 174 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 175 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 177 */ return response;
	}

	@RequestMapping(value = { "/{idApp}/{idPais}/{idioma}/check" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> checkTransaction(@PathVariable Integer idApp, @PathVariable Integer idPais,
			@PathVariable String idioma, @RequestParam Integer opId) {
		/* 186 */ ResponseEntity<Object> response = null;
		try {
			/* 188 */ response = new ResponseEntity(this.billPocketService.checkTransaction(opId), HttpStatus.OK);
		}
		/* 190 */ catch (Exception e) {
			/* 191 */ e.printStackTrace();
			/* 192 */ BaseResponse error = new BaseResponse();
			/* 193 */ error.setCode(-10);
			/* 194 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 195 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 197 */ return response;
	}
}