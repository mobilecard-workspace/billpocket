package com.addcel.bill.pocket.BillPocket.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.bill.pocket.BillPocket.bean.BaseResponse;
import com.addcel.bill.pocket.BillPocket.bean.Webhook;
import com.addcel.bill.pocket.BillPocket.service.PaymentService;
import com.addcel.bill.pocket.BillPocket.service.WebHookService;
import com.google.gson.Gson;

@Controller
public class Pay3DSecureController {
	/* 19 */ private static final Logger LOGGER = LoggerFactory.getLogger(Pay3DSecureController.class);

	private static final String PATH_REDIRECT = "/3d/response";

	private static final String PATH_WEBHOOK = "/webHook";

	/* 25 */ private Gson GSON = new Gson();

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private WebHookService webHookService;

	@RequestMapping(value = { "/3d/response" }, method = { RequestMethod.GET }, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> payment3DSecure(@RequestParam String checkout, @RequestParam String externalId,
			@RequestParam String total, @RequestParam String authorization, @RequestParam String ticketUrl,
			@RequestParam String opId, @RequestParam String maskedPAN, @RequestParam String message,
			@RequestParam String token, @RequestParam String signature) {
		/* 45 */ LOGGER.info("RECIBIENDO RESPUESTA DE BILLPOCKET - checkout: " + checkout + ", externalId: "
				+ externalId + ", total: " + total + ", authorization: " + authorization + ", ticketUrl: " + ticketUrl
				+ ", opId:" + opId + ", maskedPAN: " + maskedPAN + ", message: " + message + ", token: " + token
				+ ", signature: " + signature);

		/* 48 */ ResponseEntity<Object> response = null;

		try {
			/* 51 */ response = new ResponseEntity(this.paymentService.threeDResponse(checkout, externalId, total,
					authorization, ticketUrl, opId, maskedPAN, message, token, signature), HttpStatus.OK);

		}
		/* 54 */ catch (Exception e) {
			/* 55 */ e.printStackTrace();
			/* 56 */ BaseResponse error = new BaseResponse();
			/* 57 */ error.setCode(-10);
			/* 58 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 59 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 61 */ return response;
	}

	@RequestMapping(value = { "/webHook" }, method = { RequestMethod.POST }, produces = {
			"application/json;charset=UTF-8" })
	@ResponseBody
	public ResponseEntity<Object> webHook(@RequestBody Webhook webhook) {
		/* 67 */ LOGGER.info("RECIBIENDO RESPUESTA DE BILLPOCKET - WebHook {}", this.GSON.toJson(webhook));
		/* 68 */ ResponseEntity<Object> response = null;
		try {
			/* 70 */ BaseResponse resp = new BaseResponse();
			/* 71 */ resp.setCode(0);
			/* 72 */ resp.setMessage("Webhook Accepted");
			try {
				/* 74 */ LOGGER.info("RECIBIENDO PAGO EXITOSO - APROVISIONANDO SALDO - {}", webhook.getExternalId());
				/* 75 */ this.webHookService.processAntadPayment(webhook);
				/* 76 */ } catch (Exception e) {
				/* 77 */ e.printStackTrace();
			}
			/* 79 */ response = new ResponseEntity(resp, HttpStatus.ACCEPTED);
			/* 80 */ } catch (Exception e) {
			/* 81 */ e.printStackTrace();
			/* 82 */ BaseResponse error = new BaseResponse();
			/* 83 */ error.setCode(-10);
			/* 84 */ error.setMessage("ERROR: " + e.getLocalizedMessage());
			/* 85 */ response = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		/* 87 */ return response;
	}
}