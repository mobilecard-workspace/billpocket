package com.addcel.bill.pocket.BillPocket.model.domain;

public class BillPocketConfig {
	private Integer id;
	private String apiKey;
	private String url;

	public Integer getId() {
		/* 18 */ return this.id;
	}

	public void setId(Integer id) {
		/* 22 */ this.id = id;
	}

	public String getApiKey() {
		/* 26 */ return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		/* 30 */ this.apiKey = apiKey;
	}

	public String getUrl() {
		/* 34 */ return this.url;
	}

	public void setUrl(String url) {
		/* 38 */ this.url = url;
	}
}