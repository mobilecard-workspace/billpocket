package com.addcel.bill.pocket.BillPocket.model.domain;

public class Tarjeta {
	private int idTarjeta;
	private long idUsuario;
	private String pan;
	private String expDate;
	private String token;
	private String ct;

	public String getToken() {
		/* 24 */ return this.token;
	}

	public void setToken(String token) {
		/* 28 */ this.token = token;
	}

	public String getCt() {
		/* 32 */ return this.ct;
	}

	public void setCt(String ct) {
		/* 36 */ this.ct = ct;
	}

	public int getIdTarjeta() {
		/* 43 */ return this.idTarjeta;
	}

	public void setIdTarjeta(int idTarjeta) {
		/* 47 */ this.idTarjeta = idTarjeta;
	}

	public long getIdUsuario() {
		/* 51 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 55 */ this.idUsuario = idUsuario;
	}

	public String getPan() {
		/* 59 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 63 */ this.pan = pan;
	}

	public String getExpDate() {
		/* 67 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 71 */ this.expDate = expDate;
	}
}