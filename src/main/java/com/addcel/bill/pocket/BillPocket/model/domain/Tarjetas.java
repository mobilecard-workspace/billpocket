package com.addcel.bill.pocket.BillPocket.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tarjetas_usuario")
public class Tarjetas {
	@Id
	@Column(name = "idtarjetasusuario")
	private Integer idTarjeta;
	@Column(name = "id_aplicacion")
	private Integer idApp;
	@Column(name = "numerotarjeta")
	private String pan;
	@Column(name = "vigencia")
	private String expDate;
	@Column(name = "ct")
	private String ct;
	@Column(name = "estado")
	private String estado;

	public Integer getIdTarjeta() {
		/* 32 */ return this.idTarjeta;
	}

	public void setIdTarjeta(Integer idTarjeta) {
		/* 36 */ this.idTarjeta = idTarjeta;
	}

	public Integer getIdApp() {
		/* 40 */ return this.idApp;
	}

	public void setIdApp(Integer idApp) {
		/* 44 */ this.idApp = idApp;
	}

	public String getPan() {
		/* 48 */ return this.pan;
	}

	public void setPan(String pan) {
		/* 52 */ this.pan = pan;
	}

	public String getExpDate() {
		/* 56 */ return this.expDate;
	}

	public void setExpDate(String expDate) {
		/* 60 */ this.expDate = expDate;
	}

	public String getCt() {
		/* 64 */ return this.ct;
	}

	public void setCt(String ct) {
		/* 68 */ this.ct = ct;
	}

	public String getEstado() {
		/* 72 */ return this.estado;
	}

	public void setEstado(String estado) {
		/* 76 */ this.estado = estado;
	}
}