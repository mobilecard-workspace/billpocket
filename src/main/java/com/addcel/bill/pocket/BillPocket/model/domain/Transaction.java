package com.addcel.bill.pocket.BillPocket.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_bitacora")
public class Transaction {
	@Id
	@Column(name = "id_bitacora")
	private Long idBitacora;
	@Column(name = "id_usuario")
	private Long idUsuario;
	@Column(name = "id_aplicacion")
	private Integer idApp;
	@Column(name = "id_proveedor")
	private Integer idProveedor;
	@Column(name = "id_producto")
	private Integer idProducto;
	@Column(name = "bit_concepto")
	private String concepto;
	@Column(name = "bit_cargo")
	private Double cargo;
	@Column(name = "bit_comision")
	private Double comision;
	@Column(name = "bit_card_id")
	private Integer idTarjeta;
	@Column(name = "bit_referencia")
	private String referencia;
	@Column(name = "imei")
	private String imei;
	@Column(name = "software")
	private String software;
	@Column(name = "modelo")
	private String model;
	@Column(name = "cx")
	private Double cx;
	@Column(name = "cy")
	private Double cy;
	@Column(name = "id_establecimiento")
	private Long idEstablecimiento;
	@Column(name = "destino")
	private String destino;

	public Long getIdBitacora() {
		/* 62 */ return this.idBitacora;
	}

	public void setIdBitacora(Long idBitacora) {
		/* 66 */ this.idBitacora = idBitacora;
	}

	public Long getIdUsuario() {
		/* 70 */ return this.idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		/* 74 */ this.idUsuario = idUsuario;
	}

	public Integer getIdApp() {
		/* 78 */ return this.idApp;
	}

	public void setIdApp(Integer idApp) {
		/* 82 */ this.idApp = idApp;
	}

	public Integer getIdProveedor() {
		/* 86 */ return this.idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		/* 90 */ this.idProveedor = idProveedor;
	}

	public Integer getIdProducto() {
		/* 94 */ return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		/* 98 */ this.idProducto = idProducto;
	}

	public String getConcepto() {
		/* 102 */ return this.concepto;
	}

	public void setConcepto(String concepto) {
		/* 106 */ this.concepto = concepto;
	}

	public Double getCargo() {
		/* 110 */ return this.cargo;
	}

	public void setCargo(Double cargo) {
		/* 114 */ this.cargo = cargo;
	}

	public Integer getIdTarjeta() {
		/* 118 */ return this.idTarjeta;
	}

	public void setIdTarjeta(Integer idTarjeta) {
		/* 122 */ this.idTarjeta = idTarjeta;
	}

	public String getReferencia() {
		/* 126 */ return this.referencia;
	}

	public void setReferencia(String referencia) {
		/* 130 */ this.referencia = referencia;
	}

	public Double getComision() {
		/* 134 */ return this.comision;
	}

	public void setComision(Double comision) {
		/* 138 */ this.comision = comision;
	}

	public String getImei() {
		/* 142 */ return this.imei;
	}

	public void setImei(String imei) {
		/* 146 */ this.imei = imei;
	}

	public String getSoftware() {
		/* 150 */ return this.software;
	}

	public void setSoftware(String software) {
		/* 154 */ this.software = software;
	}

	public String getModel() {
		/* 158 */ return this.model;
	}

	public void setModel(String model) {
		/* 162 */ this.model = model;
	}

	public Double getCx() {
		/* 166 */ return this.cx;
	}

	public void setCx(Double cx) {
		/* 170 */ this.cx = cx;
	}

	public Double getCy() {
		/* 174 */ return this.cy;
	}

	public void setCy(Double cy) {
		/* 178 */ this.cy = cy;
	}

	public Long getIdEstablecimiento() {
		/* 182 */ return this.idEstablecimiento;
	}

	public void setIdEstablecimiento(Long idEstablecimiento) {
		/* 186 */ this.idEstablecimiento = idEstablecimiento;
	}

	public String getDestino() {
		/* 190 */ return this.destino;
	}

	public void setDestino(String destino) {
		/* 194 */ this.destino = destino;
	}
}