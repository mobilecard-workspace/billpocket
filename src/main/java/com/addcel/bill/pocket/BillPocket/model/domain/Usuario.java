package com.addcel.bill.pocket.BillPocket.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_usuarios")
public class Usuario {
	@Id
	@Column(name = "id_usuario")
	private long idUsuario;
	@Column(name = "usr_login")
	private String login;
	@Column(name = "usr_pwd")
	private String password;
	@Column(name = "usr_telefono")
	private String numCelular;
	@Column(name = "usr_nombre")
	private String nombre;
	@Column(name = "usr_apellido")
	private String apellidoP;
	@Column(name = "usr_materno")
	private String apellidoM;
	@Column(name = "email")
	private String email;
	@Column(name = "id_usr_status")
	private int status;

	public long getIdUsuario() {
		/* 42 */ return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		/* 46 */ this.idUsuario = idUsuario;
	}

	public String getLogin() {
		/* 50 */ return this.login;
	}

	public void setLogin(String login) {
		/* 54 */ this.login = login;
	}

	public String getPassword() {
		/* 58 */ return this.password;
	}

	public void setPassword(String password) {
		/* 62 */ this.password = password;
	}

	public String getNumCelular() {
		/* 66 */ return this.numCelular;
	}

	public void setNumCelular(String numCelular) {
		/* 70 */ this.numCelular = numCelular;
	}

	public String getNombre() {
		/* 74 */ return this.nombre;
	}

	public void setNombre(String nombre) {
		/* 78 */ this.nombre = nombre;
	}

	public String getApellidoP() {
		/* 82 */ return this.apellidoP;
	}

	public void setApellidoP(String apellidoP) {
		/* 86 */ this.apellidoP = apellidoP;
	}

	public String getApellidoM() {
		/* 90 */ return this.apellidoM;
	}

	public void setApellidoM(String apellidoM) {
		/* 94 */ this.apellidoM = apellidoM;
	}

	public String getEmail() {
		/* 98 */ return this.email;
	}

	public void setEmail(String email) {
		/* 102 */ this.email = email;
	}

	public int getStatus() {
		/* 106 */ return this.status;
	}

	public void setStatus(int status) {
		/* 110 */ this.status = status;
	}
}