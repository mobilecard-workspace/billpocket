package com.addcel.bill.pocket.BillPocket.model.repository;

import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AuthorizationRepository extends CrudRepository<Transaction, Long> {
	@Query(value = "SELECT insertatransaccion_billpocket(:nidusuario, :nidaplicacion, :nidioma, :nid_proveedor, :nid_integrador, :nid_producto, :nbit_concepto, :nreferencia, :nbit_cargo, :ncomision, :nbit_card_id,:ncardtype, :nimei, :nsoftware, :nmodelo, :nlat, :nlon, :ncardToken, :nmaskedPan, :nemisor, :noperacion)", nativeQuery = true)
	String insertatransaccion_billpocket(@Param("nidusuario") String paramString1,
			@Param("nidaplicacion") int paramInt1, @Param("nidioma") String paramString2,
			@Param("nid_proveedor") int paramInt2, @Param("nid_integrador") int paramInt3,
			@Param("nid_producto") int paramInt4, @Param("nbit_concepto") String paramString3,
			@Param("nreferencia") String paramString4, @Param("nbit_cargo") double paramDouble1,
			@Param("ncomision") double paramDouble2, @Param("nbit_card_id") int paramInt5,
			@Param("ncardtype") String paramString5, @Param("nimei") String paramString6,
			@Param("nsoftware") String paramString7, @Param("nmodelo") String paramString8,
			@Param("nlat") double paramDouble3, @Param("nlon") double paramDouble4,
			@Param("ncardToken") String paramString9, @Param("nmaskedPan") String paramString10,
			@Param("nemisor") String paramString11, @Param("noperacion") String paramString12);

	@Query(value = "SELECT insertatransaccion_manual_billpocket(:nidusuario, :nidestablecimiento, :nidaplicacion, :nidioma, :nid_proveedor, :nid_integrador, :nid_producto, :nbit_concepto, :nreferencia, :nbit_cargo, :ncomision, :npropina, :nbit_card_id, :npan, :nexpdate, :nct, :ncardtype, :nimei, :nsoftware, :nmodelo, :nlat, :nlon)", nativeQuery = true)
	String insertatransaccion_manual_billpocket(@Param("nidusuario") String paramString1,
			@Param("nidestablecimiento") long paramLong, @Param("nidaplicacion") int paramInt1,
			@Param("nidioma") String paramString2, @Param("nid_proveedor") int paramInt2,
			@Param("nid_integrador") int paramInt3, @Param("nid_producto") int paramInt4,
			@Param("nbit_concepto") String paramString3, @Param("nreferencia") String paramString4,
			@Param("nbit_cargo") double paramDouble1, @Param("ncomision") double paramDouble2,
			@Param("npropina") double paramDouble3, @Param("nbit_card_id") int paramInt5,
			@Param("npan") String paramString5, @Param("nexpdate") String paramString6,
			@Param("nct") String paramString7, @Param("ncardtype") String paramString8,
			@Param("nimei") String paramString9, @Param("nsoftware") String paramString10,
			@Param("nmodelo") String paramString11, @Param("nlat") double paramDouble4,
			@Param("nlon") double paramDouble5);

	@Query(value = "SELECT actualizatransaccion_billpocket(:idTransaccion, :status, :message, :opId,  :txnISOCode, :authNumber, :ticketUrl, :maskedPAN)", nativeQuery = true)
	String actualizatransaccion_billpocket(@Param("idTransaccion") long paramLong1, @Param("status") int paramInt,
			@Param("message") String paramString1, @Param("opId") long paramLong2,
			@Param("txnISOCode") String paramString2, @Param("authNumber") String paramString3,
			@Param("ticketUrl") String paramString4, @Param("maskedPAN") String paramString5);

	@Query(value = "SELECT actualizatransaccion_billpocket(:idTransaccion, :status, :message, :opId,  :txnISOCode, :authNumber, :ticketUrl, :maskedPAN)", nativeQuery = true)
	String actualizatransaccion_manual_billpocket();

	@Query(value = "SELECT reversatransaccion_billpocket(:idApp, :idIntegrador, :idTransaccion)", nativeQuery = true)
	String reversatransaccion_billpocket(@Param("idApp") long paramLong1, @Param("idIntegrador") long paramLong2,
			@Param("idTransaccion") long paramLong3);

	@Query(value = "SELECT actualizareverso_billpocket(:idTransaccion, :nstatus, :message, :opId, :txnISOCode ,  :authNumber, :maskedPan)", nativeQuery = true)
	String actualizareverso_billpocket(@Param("idTransaccion") long paramLong, @Param("nstatus") int paramInt1,
			@Param("message") String paramString1, @Param("opId") int paramInt2,
			@Param("txnISOCode") String paramString2, @Param("authNumber") String paramString3,
			@Param("maskedPan") String paramString4);

	Optional<Transaction> findByIdBitacora(Long paramLong);
}