package com.addcel.bill.pocket.BillPocket.model.repository;

import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
	Optional<Transaction> findByIdBitacora(long paramLong);
}