package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.antad.ProcesaAutorizacion;
import com.addcel.bill.pocket.BillPocket.antad.ProcesaAutorizacionResponse;
import com.addcel.bill.pocket.BillPocket.bean.TransactoResponse;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Service
public class AntadClient extends WebServiceGatewaySupport {
	/* 24 */ private static final Logger LOGGER = LoggerFactory.getLogger(AntadClient.class);

	/* 26 */ private Gson GSON = new Gson();

	public TransactoResponse aprovisionaServicio(long idBitacora) {
		/* 29 */ TransactoResponse response = null;
		/* 30 */ String resp = null;
		/* 31 */ ProcesaAutorizacion request = new ProcesaAutorizacion();
		/* 32 */ ProcesaAutorizacionResponse responseWS = null;
		/* 33 */ Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		try {
			/* 35 */ LOGGER.info("ENVIANDO APROVISIONAMIENTO A ANTAD - ID BITACORA - {}", Long.valueOf(idBitacora));
			/* 36 */ resp = "{\"idTransaccion\": " + idBitacora + "}";
			/* 37 */ resp = AddcelCrypto
					.encryptSensitive((new SimpleDateFormat("ddhhmmssSSS"))/* 38 */ .format(new Date()), resp);
			/* 39 */ request.setJson(resp);
			/* 40 */ marshaller
					.setClassesToBeBound(new Class[] { ProcesaAutorizacion.class, ProcesaAutorizacionResponse.class });

			/* 44 */ getWebServiceTemplate().setMarshaller((Marshaller) marshaller);
			/* 45 */ getWebServiceTemplate().setUnmarshaller((Unmarshaller) marshaller);
			/* 46 */ marshaller.afterPropertiesSet();
			/* 47 */ getWebServiceTemplate().afterPropertiesSet();

			/* 49 */ responseWS = (ProcesaAutorizacionResponse) getWebServiceTemplate()
					.marshalSendAndReceive("http://localhost/AntadBridgeWS/Services", request);

			/* 52 */ resp = responseWS.getReturn();
			/* 53 */ resp = AddcelCrypto.decryptSensitive(resp);
			/* 54 */ LOGGER.info("RESPUESTA DE ANTAD - {}", resp);
			/* 55 */ response = (TransactoResponse) this.GSON.fromJson(resp, TransactoResponse.class);
			/* 56 */ if (response.getIdError() == 0)
				;
		}
		/* 58 */ catch (Exception e) {
			/* 59 */ e.printStackTrace();
			/* 60 */ response = new TransactoResponse();
			/* 61 */ response.setIdError(-9000);
			/* 62 */ response.setMensajeError("Ocurrio un error el aprovisionar la compra.");
		}
		/* 64 */ return response;
	}
}