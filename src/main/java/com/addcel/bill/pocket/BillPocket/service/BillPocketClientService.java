package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.Authorization;
import com.addcel.bill.pocket.BillPocket.bean.MobilecardPayRequest;
import com.addcel.bill.pocket.BillPocket.bean.ResponseAuthorization;
import com.addcel.bill.pocket.BillPocket.bean.ResponseCheckout;
import com.addcel.bill.pocket.BillPocket.bean.ResponseEnrollmentCard;
import com.addcel.bill.pocket.BillPocket.bean.TransactionManualRequest;

public interface BillPocketClientService {
	ResponseEnrollmentCard enrollCard(String paramString1, String paramString2, String paramString3,
			String paramString4);

	ResponseAuthorization authorizationRequest(Authorization paramAuthorization, double paramDouble);

	ResponseAuthorization authorizationManualRequest(Authorization paramAuthorization,
			TransactionManualRequest paramTransactionManualRequest);

	ResponseAuthorization mobilecardPay(MobilecardPayRequest paramMobilecardPayRequest);

	ResponseAuthorization refundRequest(Integer paramInteger);

	ResponseAuthorization checkTransaction(Integer paramInteger);

	ResponseCheckout checkout(long paramLong, String paramString, double paramDouble);
}

/*
 * Location:
 * /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/service/
 * BillPocketClientService.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */