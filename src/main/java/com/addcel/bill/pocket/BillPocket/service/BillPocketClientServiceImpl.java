package com.addcel.bill.pocket.BillPocket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.addcel.bill.pocket.BillPocket.bean.Authorization;
import com.addcel.bill.pocket.BillPocket.bean.CardDetail;
import com.addcel.bill.pocket.BillPocket.bean.MobilecardPayRequest;
import com.addcel.bill.pocket.BillPocket.bean.RequestAmexAuthorization;
import com.addcel.bill.pocket.BillPocket.bean.RequestAuthorization;
import com.addcel.bill.pocket.BillPocket.bean.RequestCheckout;
import com.addcel.bill.pocket.BillPocket.bean.RequestEnrollmenCard;
import com.addcel.bill.pocket.BillPocket.bean.RequestManualAuthorization;
import com.addcel.bill.pocket.BillPocket.bean.RequestRefund;
import com.addcel.bill.pocket.BillPocket.bean.ResponseAuthorization;
import com.addcel.bill.pocket.BillPocket.bean.ResponseCheckout;
import com.addcel.bill.pocket.BillPocket.bean.ResponseEnrollmentCard;
import com.addcel.bill.pocket.BillPocket.bean.TransactionManualRequest;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class BillPocketClientServiceImpl implements BillPocketClientService {
	/* 20 */ private static final Logger LOGGER = LoggerFactory.getLogger(BillPocketClientServiceImpl.class);

	private static final String API_KEY = "PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg";

	private static final String URL_BP = "https://www.billpocket.com/";

	@Autowired
	private RestTemplate restTemplate;

	/* 33 */ private Gson GSON = new Gson();

	public ResponseEnrollmentCard enrollCard(String pan, String expDate, String ct, String apiKey) {
		/* 37 */ ResponseEnrollmentCard response = new ResponseEnrollmentCard();
		/* 38 */ RequestEnrollmenCard requestEnrollmenCard = null;
		/* 39 */ String exp = null;
		try {
			/* 41 */ HttpHeaders headers = new HttpHeaders();
			/* 42 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 43 */ exp = AddcelCrypto.decryptTarjeta(expDate);
			/* 44 */ exp = exp.replace("/", "");
			/* 45 */ exp = exp.substring(2, exp.length()) + exp.substring(0, 2);
			/* 46 */ requestEnrollmenCard = new RequestEnrollmenCard();
			/* 47 */ requestEnrollmenCard.setApiKey("PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg");
			/* 48 */ requestEnrollmenCard.setPan(AddcelCrypto.decryptTarjeta(pan));
			/* 49 */ requestEnrollmenCard.setExpDate(exp);
			/* 50 */ requestEnrollmenCard.setCvv2(AddcelCrypto.decryptTarjeta(ct));
			/* 51 */ LOGGER.info("FECHA VIGENCIA FORMATO BANCARIO BP : CARD: " + requestEnrollmenCard.getPan()
					+ ", EXP: " + requestEnrollmenCard/* 52 */ .getExpDate() + ", CT: "
					+ requestEnrollmenCard/* 53 */ .getCvv2());
			/* 54 */ HttpEntity<RequestEnrollmenCard> request = new HttpEntity(requestEnrollmenCard,
					(MultiValueMap) headers);
			/* 55 */ ResponseEntity<ResponseEnrollmentCard> respEnrollCard = this.restTemplate.exchange(
					"https://www.billpocket.com/scops/card", HttpMethod.POST, request, ResponseEnrollmentCard.class,
					new Object[0]);

			/* 57 */ response = (ResponseEnrollmentCard) respEnrollCard.getBody();
			/* 58 */ response.setMaskedPan(requestEnrollmenCard.getPan().substring(0, 6) + "********"
					+ requestEnrollmenCard/* 59 */ .getPan().substring(requestEnrollmenCard.getPan().length() - 4,
							requestEnrollmenCard/* 60 */ .getPan().length()));
			/* 61 */ if (response.getCode() == 0) {
				/* 62 */ response.setCode(-2000);
				/* 63 */ } else if (response.getCode() == 1) {
				/* 64 */ response.setCode(0);
			}
			/* 66 */ } catch (HttpClientErrorException e) {
			/* 67 */ LOGGER.error("La solicitud contiene sintaxis incorrecta o no puede procesarse.", (Throwable) e);
			/* 68 */ LOGGER.info("HTTP Status Code: " + e.getStatusCode());
			/* 69 */ response.setCode(-1);
			/* 70 */ response.setMessage(e.getLocalizedMessage());
		}
		/* 72 */ catch (HttpServerErrorException ex) {
			/* 73 */ LOGGER.error("El servidor falló al completar una solicitud aparentemente válida.", (Throwable) ex);
			/* 74 */ LOGGER.info("HTTP Status Code: " + ex.getStatusCode());
			/* 75 */ response.setCode(-1);
			/* 76 */ response.setMessage(ex.getLocalizedMessage());
			/* 77 */ } catch (Exception ex) {
			/* 78 */ LOGGER.error("El servidor falló al completar una solicitud aparentemente válida.", ex);
			/* 79 */ LOGGER.info("HTTP Status Code: " + ex.getLocalizedMessage());
			/* 80 */ response.setCode(-1);
			/* 81 */ response.setMessage(ex.getLocalizedMessage());
		}
		/* 83 */ LOGGER.info("Response bill pocket - {}", this.GSON.toJson(response));
		/* 84 */ return response;
	}

	public ResponseAuthorization authorizationRequest(Authorization authorizationRequest, double comision) {
		/* 89 */ ResponseAuthorization response = null;
		/* 90 */ RequestAuthorization requestVM = null;
		/* 91 */ RequestAmexAuthorization requestAmex = null;
		try {
			/* 93 */ LOGGER.info("SOLICITANDO AUTORIZACION BANCARIA - {}", this.GSON.toJson(authorizationRequest));
			/* 94 */ HttpHeaders headers = new HttpHeaders();
			/* 95 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 96 */ if (!authorizationRequest.isAmex()) {
				/* 97 */ requestVM = new RequestAuthorization();
				/* 98 */ requestVM.setCardToken(authorizationRequest.getCardToken());
				/* 99 */ requestVM.setApiKey("PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg");
				/* 100 */ requestVM.setAmount(authorizationRequest.getAmount() + comision);

				/* 102 */ HttpEntity<RequestAuthorization> request = new HttpEntity(requestVM, (MultiValueMap) headers);
				/* 103 */ LOGGER.info("SOLICITANDO AUTORIZACION PARA VISA O MASTER CARD - {}",
						this.GSON.toJson(requestVM));
				/* 104 */ ResponseEntity<String> respEnrollCard = this.restTemplate.exchange(
						"https://www.billpocket.com/scops/txn", HttpMethod.POST, request, String.class, new Object[0]);

				/* 106 */ LOGGER.info("RESPUESTA BANCARIA - {}", respEnrollCard.getBody());
				/* 107 */ response = (ResponseAuthorization) this.GSON.fromJson((String) respEnrollCard.getBody(),
						ResponseAuthorization.class);
			} else {
				/* 109 */ requestAmex = new RequestAmexAuthorization();
				/* 110 */ HttpEntity<Authorization> request = new HttpEntity(authorizationRequest,
						(MultiValueMap) headers);
				/* 111 */ ResponseEntity<String> respEnrollCard = this.restTemplate.exchange(
						"https://www.billpocket.com/scops/txn", HttpMethod.POST, request, String.class, new Object[0]);

				/* 113 */ LOGGER.info("RESPUESTA BANCARIA - {}", respEnrollCard.getBody());
				/* 114 */ response = (ResponseAuthorization) this.GSON.fromJson((String) respEnrollCard.getBody(),
						ResponseAuthorization.class);
			}
			/* 116 */ } catch (ResourceAccessException e) {
			/* 117 */ e.printStackTrace();
			/* 118 */ response = new ResponseAuthorization();
			/* 119 */ response.setCode(-1);
			/* 120 */ response.setMessage(
					"Disculpe las molestias, no se hemos podido establecer comunicacion con el Banco autorizador. ");
			/* 121 */ } catch (Exception e) {
			/* 122 */ e.printStackTrace();
			/* 123 */ response = new ResponseAuthorization();
			/* 124 */ response.setCode(-1);
			/* 125 */ response.setMessage(e.getLocalizedMessage());
		}
		/* 127 */ return response;
	}

	public ResponseAuthorization authorizationManualRequest(Authorization authorizationRequest,
			TransactionManualRequest transaction) {
		/* 132 */ ResponseAuthorization response = null;
		/* 133 */ RequestManualAuthorization requestVM = null;
		/* 134 */ RequestAmexAuthorization requestAmex = null;
		/* 135 */ CardDetail card = null;
		/* 136 */ String pan = null;
		/* 137 */ String expDate = null;
		try {
			/* 139 */ LOGGER.info("SOLICITANDO AUTORIZACION BANCARIA  - {}", this.GSON.toJson(authorizationRequest));
			/* 140 */ HttpHeaders headers = new HttpHeaders();
			/* 141 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 142 */ if (!authorizationRequest.isAmex()) {
				/* 143 */ requestVM = new RequestManualAuthorization();
				/* 144 */ requestVM.setApiKey("PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg");
				/* 145 */ requestVM.setAmount(authorizationRequest.getAmount());
				/* 146 */ card = new CardDetail();
				/* 147 */ pan = AddcelCrypto.decryptTarjeta(transaction.getPan());
				/* 148 */ card.setPan(pan);
				/* 149 */ expDate = AddcelCrypto.decryptTarjeta(transaction.getExpDate());
				/* 150 */ expDate = expDate.replace("/", "");
				/* 151 */ card.setExpDate(expDate);
				/* 152 */ card.setCvv2(AddcelCrypto.decryptTarjeta(transaction.getCt()));
				/* 153 */ requestVM.setCardReq(card);
				/* 154 */ HttpEntity<RequestManualAuthorization> request = new HttpEntity(requestVM,
						(MultiValueMap) headers);
				/* 155 */ LOGGER.info("SOLICITANDO AUTORIZACION PARA VISA O MASTER CARD - {}",
						this.GSON.toJson(requestVM));
				/* 156 */ ResponseEntity<String> respEnrollCard = this.restTemplate.exchange(
						"https://www.billpocket.com/scops/txn", HttpMethod.POST, request, String.class, new Object[0]);

				/* 158 */ LOGGER.info("RESPUESTA BANCARIA - {}", respEnrollCard.getBody());
				/* 159 */ response = (ResponseAuthorization) this.GSON.fromJson((String) respEnrollCard.getBody(),
						ResponseAuthorization.class);
			}
			/* 161 */ } catch (ResourceAccessException e) {
			/* 162 */ e.printStackTrace();
			/* 163 */ response = new ResponseAuthorization();
			/* 164 */ response.setCode(-1);
			/* 165 */ response.setMessage(
					"Disculpe las molestias, no se hemos podido establecer comunicacion con el Banco autorizador. ");
			/* 166 */ } catch (Exception e) {
			/* 167 */ e.printStackTrace();
			/* 168 */ response = new ResponseAuthorization();
			/* 169 */ response.setCode(-1);
			/* 170 */ response.setMessage(e.getLocalizedMessage());
		}
		/* 172 */ return response;
	}

	public ResponseAuthorization mobilecardPay(MobilecardPayRequest transaction) {
		/* 177 */ ResponseAuthorization response = new ResponseAuthorization();
		/* 178 */ RequestManualAuthorization requestVM = null;
		/* 179 */ CardDetail card = null;
		/* 180 */ String pan = null;
		try {
			/* 182 */ LOGGER.info("SOLICITANDO AUTORIZACION BANCARIA MOBILECARD PAY - {}",
					this.GSON.toJson(transaction));
			/* 183 */ HttpHeaders headers = new HttpHeaders();
			/* 184 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 185 */ requestVM = new RequestManualAuthorization();
			/* 186 */ requestVM.setApiKey("PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg");
			/* 187 */ requestVM.setAmount(transaction.getCargo() + transaction.getComision());
			/* 188 */ card = new CardDetail();
			/* 189 */ pan = AddcelCrypto.decryptTarjeta(transaction.getPan());
			/* 190 */ card.setPan(pan.substring(4, 6) + "/" + pan.substring(0, 2));
			/* 191 */ card.setExpDate(AddcelCrypto.decryptTarjeta(transaction.getExpDate()));
			/* 192 */ card.setCvv2(AddcelCrypto.decryptTarjeta(transaction.getCt()));
			/* 193 */ requestVM.setCardReq(card);
			/* 194 */ HttpEntity<RequestManualAuthorization> request = new HttpEntity(requestVM,
					(MultiValueMap) headers);
			/* 195 */ LOGGER.info("SOLICITANDO AUTORIZACION PARA VISA O MASTER CARD - {}", this.GSON.toJson(requestVM));
			/* 196 */ ResponseEntity<String> respEnrollCard = this.restTemplate.exchange(
					"https://www.billpocket.com/scops/txn", HttpMethod.POST, request, String.class, new Object[0]);

			/* 198 */ LOGGER.info("RESPUESTA BANCARIA - {}", respEnrollCard.getBody());
			/* 199 */ response = (ResponseAuthorization) this.GSON.fromJson((String) respEnrollCard.getBody(),
					ResponseAuthorization.class);
			/* 200 */ } catch (Exception e) {
			/* 201 */ e.printStackTrace();
			/* 202 */ response = new ResponseAuthorization();
			/* 203 */ response.setCode(-1);
			/* 204 */ response.setMessage("FALLO EN EL PROCESADOR DE PAGO. INTENTE DE NUEVO.");
		}
		/* 206 */ return response;
	}

	public ResponseAuthorization refundRequest(Integer opId) {
		/* 212 */ ResponseAuthorization response = null;
		/* 213 */ RequestRefund refund = new RequestRefund();
		try {
			/* 215 */ LOGGER.info("SOLICITANDO REVERSO BANCARIO - {}", opId);
			/* 216 */ HttpHeaders headers = new HttpHeaders();
			/* 217 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 218 */ refund.setApiKey("PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg");
			/* 219 */ refund.setOpId(opId);
			/* 220 */ HttpEntity<RequestRefund> request = new HttpEntity(refund, (MultiValueMap) headers);
			/* 221 */ ResponseEntity<String> respReverse = this.restTemplate.exchange(
					"https://www.billpocket.com/scops/txn/refund", HttpMethod.POST, request, String.class,
					new Object[0]);

			/* 223 */ LOGGER.info("RESPUESTA BANCARIA - {}", respReverse.getBody());
			/* 224 */ response = (ResponseAuthorization) this.GSON.fromJson((String) respReverse.getBody(),
					ResponseAuthorization.class);
			/* 225 */ } catch (Exception e) {
			/* 226 */ e.printStackTrace();
		}
		/* 228 */ return response;
	}

	public ResponseAuthorization checkTransaction(Integer opId) {
		/* 233 */ ResponseAuthorization response = null;
		/* 234 */ RequestRefund refund = new RequestRefund();
		try {
			/* 236 */ LOGGER.info("CHECK TRANSACTION BANCARIA - {}", opId);
			/* 237 */ HttpHeaders headers = new HttpHeaders();
			/* 238 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 239 */ refund.setApiKey("PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg");
			/* 240 */ refund.setOpId(opId);
			/* 241 */ HttpEntity<RequestRefund> request = new HttpEntity(refund, (MultiValueMap) headers);
			/* 242 */ ResponseEntity<String> respReverse = this.restTemplate.exchange(
					"https://www.billpocket.com/scops/txn/" + opId + "?api_key="
							+ "PjS4WB93kOtnvQUbXl9GogAAJR5TAVC-0vyaXjzCG_QtszRSAAF-vg",
					HttpMethod.GET, request, String.class, new Object[0]);

			/* 245 */ LOGGER.info("RESPUESTA CHECK TRANSACTION BANCARIA - {}", respReverse.getBody());
			/* 246 */ response = (ResponseAuthorization) this.GSON.fromJson((String) respReverse.getBody(),
					ResponseAuthorization.class);
			/* 247 */ } catch (Exception e) {
			/* 248 */ e.printStackTrace();
		}
		/* 250 */ return response;
	}

	public ResponseCheckout checkout(long idTransaccion, String concepto, double monto) {
		/* 255 */ ResponseCheckout response = null;
		/* 256 */ RequestCheckout checkout = new RequestCheckout();
		/* 257 */ String[] item = new String[1];
		try {
			/* 259 */ LOGGER.info("CHECK OUT 3D SECURE  - {}", Long.valueOf(idTransaccion));
			/* 260 */ HttpHeaders headers = new HttpHeaders();
			/* 261 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 262 */ checkout.setApiKey("R4TEKWZ-DNC4C9G-HMEVK7D-4SZ5D78");
			/* 263 */ checkout.setExternalId(String.valueOf(idTransaccion));
			/* 264 */ item[0] = concepto;
			/* 265 */ checkout.setItems(item);
			/* 266 */ checkout.setTotal(monto);
			/* 267 */ HttpEntity<RequestCheckout> request = new HttpEntity(checkout, (MultiValueMap) headers);
			/* 268 */ ResponseEntity<String> respReverse = this.restTemplate.exchange(
					"https://paywith.billpocket.com/api/v1/checkout", HttpMethod.GET, request, String.class,
					new Object[0]);

			/* 271 */ LOGGER.info("RESPUESTA CHECK TRANSACTION BANCARIA - {}", respReverse.getBody());
			/* 272 */ response = (ResponseCheckout) this.GSON.fromJson((String) respReverse.getBody(),
					ResponseCheckout.class);
			/* 273 */ } catch (Exception e) {
			/* 274 */ e.printStackTrace();
		}
		/* 276 */ return response;
	}
}