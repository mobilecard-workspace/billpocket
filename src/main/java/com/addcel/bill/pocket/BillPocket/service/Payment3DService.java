package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.ResponseCheckout;
import com.addcel.bill.pocket.BillPocket.bean.TransactionRequest;

public interface Payment3DService {
	ResponseCheckout checkout(Integer paramInteger1, Integer paramInteger2, String paramString,
			TransactionRequest paramTransactionRequest);
}