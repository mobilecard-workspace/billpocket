package com.addcel.bill.pocket.BillPocket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.bill.pocket.BillPocket.bean.Authorization;
import com.addcel.bill.pocket.BillPocket.bean.ResponseCheckout;
import com.addcel.bill.pocket.BillPocket.bean.TransactionRequest;
import com.addcel.bill.pocket.BillPocket.model.repository.AuthorizationRepository;
import com.addcel.bill.pocket.BillPocket.model.repository.TarjetasRepository;
import com.google.gson.Gson;

@Service
public class Payment3DServiceImpl implements Payment3DService {
	/* 24 */ private static final Logger LOGGER = LoggerFactory.getLogger(Payment3DServiceImpl.class);

	/* 26 */ private static final Integer CODE_SUCESS_BP = Integer.valueOf(0);

	@Autowired
	private AuthorizationRepository authorizationRepository;

	@Autowired
	private TarjetasRepository tarjetasRepository;

	@Autowired
	private BillPocketClientServiceImpl billPocketClient;

	/* 37 */ private Gson GSON = new Gson();

	public ResponseCheckout checkout(Integer idApp, Integer idPais, String idioma, TransactionRequest transaction) {
		/* 41 */ ResponseCheckout checkout = null;
		/* 42 */ String callTransaccion = null;
		/* 43 */ Authorization authorization = null;
		try {
			/* 45 */ checkout = this.billPocketClient.checkout(100L, "Prueba", 10.0D);

		}
		/* 63 */ catch (Exception e) {
			/* 64 */ checkout = new ResponseCheckout();
			/* 65 */ checkout.setCode(-1000);
			/* 66 */ checkout.setMessage("La transaccion no puede ser procesada.");
			/* 67 */ e.printStackTrace();
		}
		/* 69 */ return null;
	}
}