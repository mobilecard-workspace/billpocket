package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.MobilecardPayRequest;
import com.addcel.bill.pocket.BillPocket.bean.ResponseAuthorization;
import com.addcel.bill.pocket.BillPocket.bean.ResponseEnrollmentCard;
import com.addcel.bill.pocket.BillPocket.bean.TransactionManualRequest;
import com.addcel.bill.pocket.BillPocket.bean.TransactionRequest;

public interface PaymentService {
	ResponseEnrollmentCard cardTokenization(Integer paramInteger1, Integer paramInteger2, String paramString,
			long paramLong, Integer paramInteger3);

	String cardTokenValidation(Integer paramInteger);

	ResponseAuthorization authorization(Integer paramInteger1, Integer paramInteger2, String paramString,
			TransactionRequest paramTransactionRequest);

	ResponseAuthorization authorizationManualRequest(Integer paramInteger1, Integer paramInteger2, String paramString,
			TransactionManualRequest paramTransactionManualRequest);

	ResponseAuthorization authorizationById(Integer paramInteger1, Integer paramInteger2, String paramString,
			TransactionManualRequest paramTransactionManualRequest);

	ResponseAuthorization mobilecardPayRequest(Integer paramInteger1, Integer paramInteger2, String paramString,
			MobilecardPayRequest paramMobilecardPayRequest);

	ResponseAuthorization refund(Integer paramInteger1, Integer paramInteger2, String paramString, long paramLong);

	ResponseAuthorization checkTransaction(Integer paramInteger1, Integer paramInteger2, String paramString,
			Integer paramInteger3);

	ResponseAuthorization authorizationByUserScanRequest(Integer paramInteger1, Integer paramInteger2,
			String paramString, TransactionRequest paramTransactionRequest);

	ResponseAuthorization threeDResponse(String paramString1, String paramString2, String paramString3,
			String paramString4, String paramString5, String paramString6, String paramString7, String paramString8,
			String paramString9, String paramString10);
}

/*
 * Location:
 * /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/service/
 * PaymentService.class Java compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */