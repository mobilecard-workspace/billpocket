package com.addcel.bill.pocket.BillPocket.service;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.addcel.bill.pocket.BillPocket.bean.Authorization;
import com.addcel.bill.pocket.BillPocket.bean.BaseResponse;
import com.addcel.bill.pocket.BillPocket.bean.MobilecardPayRequest;
import com.addcel.bill.pocket.BillPocket.bean.Refund;
import com.addcel.bill.pocket.BillPocket.bean.ResponseAuthorization;
import com.addcel.bill.pocket.BillPocket.bean.ResponseEnrollmentCard;
import com.addcel.bill.pocket.BillPocket.bean.TransactionManualRequest;
import com.addcel.bill.pocket.BillPocket.bean.TransactionRequest;
import com.addcel.bill.pocket.BillPocket.model.domain.BillPocketConfig;
import com.addcel.bill.pocket.BillPocket.model.domain.Tarjeta;
import com.addcel.bill.pocket.BillPocket.model.domain.Tarjetas;
import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import com.addcel.bill.pocket.BillPocket.model.repository.AuthorizationRepository;
import com.addcel.bill.pocket.BillPocket.model.repository.TarjetasRepository;
import com.addcel.bill.pocket.BillPocket.model.repository.TransactionRepository;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class PaymentServiceImpl implements PaymentService {
	/* 26 */ private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServiceImpl.class);

	/* 28 */ private static final Integer CODE_SUCESS_BP = Integer.valueOf(0);

	@Autowired
	private AuthorizationRepository authorizationRepository;

	@Autowired
	private TarjetasRepository tarjetasRepository;

	@Autowired
	private BillPocketClientServiceImpl billPocketClient;

	@Autowired
	private TransactionRepository transactionRepository;

	/* 42 */ private Gson GSON = new Gson();

	public ResponseEnrollmentCard cardTokenization(Integer idApp, Integer idPais, String idioma, long idUsuario,
			Integer idTarjeta) {
		/* 49 */ ResponseEnrollmentCard response = new ResponseEnrollmentCard();
		try {
			/* 51 */ BillPocketConfig config = new BillPocketConfig();
			/* 52 */ Tarjeta tarjeta = new Tarjeta();
			/* 53 */ response = this.billPocketClient.enrollCard(tarjeta.getPan(), tarjeta.getExpDate(),
					tarjeta.getCt(), config.getApiKey());
			/* 54 */ } catch (HttpClientErrorException e) {
			/* 55 */ LOGGER.error("La solicitud contiene sintaxis incorrecta o no puede procesarse.", (Throwable) e);
			/* 56 */ LOGGER.info("HTTP Status Code: " + e.getStatusCode());
			/* 57 */ response.setCode(-1);
			/* 58 */ response.setMessage("Error 500 - Al invocar Procesar el pago");
		}
		/* 60 */ catch (HttpServerErrorException e) {
			/* 61 */ LOGGER.error("El servidor falló al completar una solicitud aparentemente válida.", (Throwable) e);
			/* 62 */ LOGGER.info("HTTP Status Code: " + e.getStatusCode());
			/* 63 */ response.setCode(-1);
			/* 64 */ response.setMessage("Error 500 - Al invocar Procesar el pago");
		}

		/* 67 */ return response;
	}

	public ResponseAuthorization authorization(Integer idApp, Integer idPais, String idioma,
			TransactionRequest transaction) {
		/* 73 */ ResponseAuthorization response = new ResponseAuthorization();
		/* 74 */ ResponseEnrollmentCard enrollmentCard = null;
		/* 75 */ Authorization authorization = null;
		/* 76 */ String callTransaccion = null;
		/* 77 */ BaseResponse actualizaTran = null;
		/* 78 */ LOGGER.info("SOLICITANDO LA AUTORIZACION BANCARIA CON BILL POCKET - {}",
				this.GSON.toJson(transaction));
		try {
			/* 80 */ callTransaccion = this.authorizationRepository.insertatransaccion_billpocket(
					String.valueOf(transaction.getIdUsuario()), idApp/* 81 */ .intValue(), transaction.getIdioma(),
					transaction.getIdProveedor(), 9, transaction.getIdProducto(), transaction/* 82 */ .getConcepto(),
					transaction.getReferencia(), transaction.getCargo(), transaction/* 83 */ .getComision(),
					transaction.getIdTarjeta(), transaction.getTipoTarjeta(), transaction/* 84 */ .getImei(),
					transaction.getSoftware(), transaction.getModelo(), transaction.getLat(),
					transaction/* 85 */ .getLon(), null, null, transaction.getOperacion(), transaction.getEmisor());
			/* 86 */ LOGGER.info("RESPUESTA DE DB: {}", callTransaccion);
			/* 87 */ if (callTransaccion != null) {
				/* 88 */ authorization = (Authorization) this.GSON.fromJson(callTransaccion, Authorization.class);
				/* 89 */ if (authorization.getCardToken() != null) {
					/* 90 */ LOGGER.info("TARJETA TOKENIZADA. SE ENVIA A PAGO - {}", this.GSON.toJson(authorization));
					/* 91 */ response = this.billPocketClient.authorizationRequest(authorization,
							transaction.getComision());
					/* 92 */ LOGGER.info("RESPUESTA DE AUTORIZACION - {}", this.GSON.toJson(response));
					/* 93 */ LOGGER.info("ACTUALIZA TRANSACION PARAMS -  ID TRANSACCION: "
							+ authorization/* 94 */ .getIdTransaccion() + " MESSAGE: " + response/* 95 */ .getMessage()
							+ " OPID: " + response/* 96 */ .getOpId() + " TXNISOCODE: "
							+ response/* 97 */ .getTxnISOCode() + " AUTH NUMBER: " + response/* 98 */ .getAuthNumber()
							+ " TICKET: " + response/* 99 */ .getTicketUrl() + " MASKED PAN: "
							+ response/* 100 */ .getMaskedPAN());
					/* 101 */ if (response.getStatus().intValue() != 1) {
						/* 102 */ response.setCode(-1);
					} else {
						/* 104 */ response.setCode(0);
						/* 105 */ response.setStatus(Integer.valueOf(1));
					}
					/* 107 */ callTransaccion = this.authorizationRepository.actualizatransaccion_billpocket(
							authorization/* 108 */ .getIdTransaccion(), response/* 109 */ .getStatus().intValue(),
							response/* 110 */ .getMessage(),
							/* 111 */ (response.getOpId() != null) ? response.getOpId().intValue() : 0L,
							/* 112 */ (response.getTxnISOCode() != null) ? response.getTxnISOCode() : "",
							/* 113 */ (response.getAuthNumber() != null) ? response.getAuthNumber() : "",
							/* 114 */ (response.getTicketUrl() != null) ? response.getTicketUrl() : "",
							/* 115 */ (response.getMaskedPAN() != null) ? response.getMaskedPAN() : "");
					/* 116 */ LOGGER.info("RESPUESTA DE ACTUALIZACION DE BD - {}", callTransaccion);
					/* 117 */ actualizaTran = (BaseResponse) this.GSON.fromJson(callTransaccion, BaseResponse.class);
					/* 118 */ response.setIdTransaccion(authorization.getIdTransaccion());
				}
				/* 120 */ else if (authorization.getCode() == 0) {
					/* 121 */ LOGGER.info("TARJETA NO TOKENIZADA - SOLICITANDO TOKENIZACION - {}",
							this.GSON.toJson(authorization));
					/* 122 */ enrollmentCard = this.billPocketClient.enrollCard(authorization.getPan(),
							authorization.getExpDate(), authorization/* 123 */ .getCvv2(), "");
					/* 124 */ LOGGER.info("RESPUESTA DE TOKENIZACION DE TARJETA - {}",
							this.GSON.toJson(enrollmentCard));
					/* 125 */ if (CODE_SUCESS_BP.intValue() == enrollmentCard.getCode()
							|| enrollmentCard.getCardToken() != null) {
						/* 126 */ LOGGER.info("INSERTANDO TRANSACCION - ID USUARIO - {}",
								Long.valueOf(transaction.getIdUsuario()));
						/* 127 */ callTransaccion = this.authorizationRepository.insertatransaccion_billpocket(
								String.valueOf(transaction.getIdUsuario()), idApp/* 128 */ .intValue(),
								transaction.getIdioma(), transaction.getIdProveedor(), 9, transaction.getIdProducto(),
								transaction/* 129 */ .getConcepto(), transaction.getReferencia(),
								transaction.getCargo(), transaction/* 130 */ .getComision(), transaction.getIdTarjeta(),
								transaction.getTipoTarjeta(), transaction/* 131 */ .getImei(),
								transaction.getSoftware(), transaction.getModelo(), transaction/* 132 */ .getLat(),
								transaction.getLon(), enrollmentCard.getCardToken(),
								enrollmentCard/* 133 */ .getMaskedPan(), transaction.getOperacion(),
								transaction.getEmisor());
						/* 134 */ LOGGER.info("RESPUESTA DE DB: {}", callTransaccion);
						/* 135 */ if (callTransaccion != null) {
							/* 136 */ authorization.setCardToken(enrollmentCard.getCardToken());
							/* 137 */ LOGGER.info("DATOS DE TRANSACCION - {} - CARDTOKEN - {}",
									this.GSON.toJson(transaction), enrollmentCard/* 138 */ .getCardToken()
											+ " MASKED CARD -" + enrollmentCard.getMaskedPan());
							/* 139 */ authorization = (Authorization) this.GSON.fromJson(callTransaccion,
									Authorization.class);
							/* 140 */ response = this.billPocketClient.authorizationRequest(authorization,
									transaction.getComision());
							/* 141 */ LOGGER.info("ACTUALIZA TRANSACION PARAMS -  ID TRANSACCION: "
									+ authorization/* 142 */ .getIdTransaccion() + " MESSAGE: "
									+ response/* 143 */ .getMessage() + " OPID: " + response/* 144 */ .getOpId()
									+ " TXNISOCODE: " + response/* 145 */ .getTxnISOCode() + " AUTH NUMBER: "
									+ response/* 146 */ .getAuthNumber() + " TICKET: "
									+ response/* 147 */ .getTicketUrl() + " MASKED PAN: "
									+ response/* 148 */ .getMaskedPAN());
							/* 149 */ if (response.getStatus().intValue() != 1) {
								/* 150 */ response.setCode(-1);
							} else {
								/* 152 */ response.setCode(0);
								/* 153 */ response.setStatus(Integer.valueOf(1));
							}
							/* 155 */ this.authorizationRepository.actualizatransaccion_billpocket(
									authorization.getIdTransaccion(), response/* 156 */ .getStatus().intValue(),
									response/* 157 */ .getMessage(),
									/* 158 */ (response.getOpId() != null) ? response.getOpId().intValue() : 0L,
									/* 159 */ (response.getTxnISOCode() != null) ? response.getTxnISOCode() : "",
									/* 160 */ (response.getAuthNumber() != null) ? response.getAuthNumber() : "",
									/* 161 */ (response.getTicketUrl() != null) ? response.getTicketUrl() : "",
									/* 162 */ (response.getMaskedPAN() != null) ? response.getMaskedPAN() : "");
							/* 163 */ LOGGER.info("RESPUESTA DE ACTUALIZACION DE BD - {}", callTransaccion);
							/* 164 */ actualizaTran = (BaseResponse) this.GSON.fromJson(callTransaccion,
									BaseResponse.class);
							/* 165 */ response.setIdTransaccion(authorization.getIdTransaccion());
						} else {
							/* 167 */ response.setCode(-50);
							/* 168 */ response.setMessage("ERROR AL GUARDAR LA TRANSACION EN BD");
						}
					} else {
						/* 171 */ response.setCode(enrollmentCard.getCode());
						/* 172 */ response.setMessage(enrollmentCard.getMessage());
					}
				} else {
					/* 175 */ response.setCode(authorization.getCode());
					/* 176 */ response.setMessage(authorization.getMessage());
				}

				/* 179 */ LOGGER.info("RESPUESTA DE BILL POCKET . {}", this.GSON.toJson(response));
			} else {
				/* 181 */ response = new ResponseAuthorization();
				/* 182 */ response.setCode(-50);
				/* 183 */ response.setMessage("ERROR AL GUARDAR LA TRANSACION EN BD");
			}
			/* 185 */ } catch (Exception e) {
			/* 186 */ e.printStackTrace();
		}
		/* 188 */ return response;
	}

	public ResponseAuthorization authorizationManualRequest(Integer idApp, Integer idPais, String idioma,
			TransactionManualRequest transaction) {
		/* 194 */ ResponseAuthorization response = new ResponseAuthorization();
		/* 195 */ ResponseEnrollmentCard enrollCard = null;
		/* 196 */ Authorization authorization = null;
		/* 197 */ String callTransaccion = null;
		/* 198 */ BaseResponse actualizaTran = null;
		/* 199 */ LOGGER.info("SOLICITANDO LA AUTORIZACION BANCARIA MANUAL - {}", this.GSON.toJson(transaction));
		try {
			/* 201 */ LOGGER.debug("ID USUARIO: " + String.valueOf(transaction.getIdUsuario()) + " ID ESTABLECIMIENTO: "
					+ transaction/* 202 */ .getIdEstablecimiento() + " ID APP: " + idApp + " ID USUARIO: " + idioma
					+ " PROVEEDOR : " + transaction

							/* 205 */ .getIdProveedor()
					+ " ID INTEGRADOR:  9  ID PRODUCTO: " + transaction

							/* 208 */ .getIdProducto()
					+ " CONCEPTO: " + transaction/* 209 */ .getConcepto() + " REFERNCIA: "
					+ transaction/* 210 */ .getReferencia() + " CARGO: " + transaction/* 211 */ .getCargo()
					+ " COMISION: " + transaction/* 212 */ .getComision() + " PROPINA: "
					+ transaction/* 213 */ .getPropina() + " ID TARJETA: " + transaction/* 214 */ .getIdTarjeta()
					+ " PAN: " +
					/* 215 */ AddcelCrypto.decryptHard(transaction.getPan()) + " EXP DATE: " +
					/* 216 */ AddcelCrypto.decryptHard(transaction.getExpDate()) + " CT: " +
					/* 217 */ AddcelCrypto.decryptHard(transaction.getCt()) + " TIPO TARJETA: "
					+ transaction/* 218 */ .getTipoTarjeta() + " IMEI: " + transaction/* 219 */ .getImei() + " SW: "
					+ transaction/* 220 */ .getSoftware() + " MODELO: " + transaction/* 221 */ .getModelo() + " LAT: "
					+ transaction/* 222 */ .getLat() + " LON: " + transaction/* 223 */ .getLon());
			/* 224 */ callTransaccion = this.authorizationRepository.insertatransaccion_manual_billpocket(
					String.valueOf(transaction.getIdUsuario()), transaction/* 225 */ .getIdEstablecimiento(),
					idApp.intValue(), idioma, transaction.getIdProveedor(), 9, transaction/* 226 */ .getIdProducto(),
					transaction.getConcepto(), transaction.getReferencia(), transaction.getCargo(),
					transaction/* 227 */ .getComision(), transaction.getPropina(), transaction.getIdTarjeta(),
					transaction.getPan(), transaction/* 228 */ .getExpDate(), transaction.getCt(),
					transaction.getTipoTarjeta(), transaction.getImei(), transaction/* 229 */ .getSoftware(),
					transaction.getModelo(), transaction.getLat(), transaction.getLon());
			/* 230 */ LOGGER.info("RESPUESTA DE DB: {}", callTransaccion);
			/* 231 */ if (callTransaccion != null) {
				/* 232 */ authorization = (Authorization) this.GSON.fromJson(callTransaccion, Authorization.class);
				/* 233 */ enrollCard = this.billPocketClient.enrollCard(
						AddcelCrypto.encryptTarjeta(AddcelCrypto.decryptHard(transaction.getPan())),
						/* 234 */ AddcelCrypto.encryptTarjeta(AddcelCrypto.decryptHard(transaction.getExpDate())),
						/* 235 */ AddcelCrypto.encryptTarjeta(AddcelCrypto.decryptHard(transaction.getCt())),
						authorization/* 236 */ .getApiKey());
				/* 237 */ if (enrollCard.getCode() == 0 || enrollCard.getCardToken() != null) {
					/* 238 */ authorization.setAmount(transaction.getCargo() + transaction.getComision());
					/* 239 */ authorization.setCardToken(enrollCard.getCardToken());
					/* 240 */ LOGGER.info("RESPUESTA DE ENROLLCARD - {}", this.GSON.toJson(authorization));
					/* 241 */ response = this.billPocketClient.authorizationRequest(authorization,
							transaction.getComision());

					/* 243 */ LOGGER.info("RESPUESTA DE AUTORIZACION  MANUAL - {}", this.GSON.toJson(response));
					/* 244 */ LOGGER.info("ACTUALIZA TRANSACION PARAMS -  ID TRANSACCION: "
							+ authorization/* 245 */ .getIdTransaccion() + " MESSAGE: "
							+ response/* 246 */ .getMessage() + " OPID: " + response/* 247 */ .getOpId()
							+ " TXNISOCODE: " + response/* 248 */ .getTxnISOCode() + " AUTH NUMBER: "
							+ response/* 249 */ .getAuthNumber() + " TICKET: " + response/* 250 */ .getTicketUrl()
							+ " MASKED PAN: " + response/* 251 */ .getMaskedPAN());
					/* 252 */ if (response.getStatus().intValue() != 1) {
						/* 253 */ response.setCode(-1);
					} else {
						/* 255 */ response.setCode(0);
						/* 256 */ response.setStatus(Integer.valueOf(0));
					}
					/* 258 */ callTransaccion = this.authorizationRepository.actualizatransaccion_billpocket(
							authorization.getIdTransaccion(), response/* 259 */ .getStatus().intValue(),
							response/* 260 */ .getMessage(),
							/* 261 */ (response.getOpId() != null) ? response.getOpId().intValue() : 0L,
							/* 262 */ (response.getTxnISOCode() != null) ? response.getTxnISOCode() : "",
							/* 263 */ (response.getAuthNumber() != null) ? response.getAuthNumber() : "",
							/* 264 */ (response.getTicketUrl() != null) ? response.getTicketUrl() : "",
							/* 265 */ (response.getMaskedPAN() != null) ? response.getMaskedPAN() : "");
					/* 266 */ LOGGER.info("RESPUESTA DE ACTUALIZACION DE BD - {}", callTransaccion);
					/* 267 */ actualizaTran = (BaseResponse) this.GSON.fromJson(callTransaccion, BaseResponse.class);
					/* 268 */ response.setIdTransaccion(authorization.getIdTransaccion());
					/* 269 */ LOGGER.info("RESPUESTA DE PAGO MANUAL - {}", this.GSON.toJson(response));
				} else {
					/* 271 */ response.setCode(-50);
					/* 272 */ response.setMessage(enrollCard.getMessage());
				}
			} else {
				/* 275 */ response.setCode(-50);
				/* 276 */ response.setMessage("ERROR AL GUARDAR LA TRANSACION EN BD");
			}
			/* 278 */ } catch (Exception e) {
			/* 279 */ e.printStackTrace();
		}
		/* 281 */ return response;
	}

	public ResponseAuthorization authorizationById(Integer idApp, Integer idPais, String idioma,
			TransactionManualRequest transaction) {
		/* 287 */ ResponseAuthorization response = new ResponseAuthorization();
		/* 288 */ Authorization authorizationRequest = null;
		/* 289 */ Optional<Transaction> bitacora = null;
		/* 290 */ Optional<Tarjetas> tarjeta = null;
		/* 291 */ String callTransaccion = null;
		try {
			/* 293 */ LOGGER.info("SOLICITANDO LA AUTORIZACION BANCARIA POR ID - {}", this.GSON.toJson(transaction));
			/* 294 */ bitacora = this.authorizationRepository
					.findByIdBitacora(Long.valueOf(transaction.getIdBitacora()));
			/* 295 */ if (bitacora.isPresent()) {
				/* 296 */ LOGGER.info("BITACORA - {}", this.GSON.toJson(bitacora));
				/* 297 */ tarjeta = this.tarjetasRepository
						.findByIdTarjeta(Integer.valueOf(transaction.getIdTarjeta()));
				/* 298 */ if (tarjeta.isPresent()) {
					/* 299 */ LOGGER.info("TARJETA - {}", this.GSON.toJson(tarjeta));
					/* 300 */ authorizationRequest = new Authorization();
					/* 301 */ authorizationRequest.setAmount(((Transaction) bitacora.get()).getCargo().doubleValue());
					/* 302 */ transaction.setPan(((Tarjetas) tarjeta.get()).getPan());
					/* 303 */ transaction.setExpDate(((Tarjetas) tarjeta.get()).getExpDate());
					/* 304 */ transaction.setCt(((Tarjetas) tarjeta.get()).getCt());
					/* 305 */ response = this.billPocketClient.authorizationManualRequest(authorizationRequest,
							transaction);
					/* 306 */ if (response.getStatus().intValue() != 1) {
						/* 307 */ response.setCode(-1);
					} else {
						/* 309 */ response.setCode(0);
						/* 310 */ response.setStatus(Integer.valueOf(0));
					}
					/* 312 */ callTransaccion = this.authorizationRepository.actualizatransaccion_billpocket(
							transaction.getIdBitacora(), response/* 313 */ .getStatus().intValue(),
							response/* 314 */ .getMessage(),
							/* 315 */ (response.getOpId() != null) ? response.getOpId().intValue() : 0L,
							/* 316 */ (response.getTxnISOCode() != null) ? response.getTxnISOCode() : "",
							/* 317 */ (response.getAuthNumber() != null) ? response.getAuthNumber() : "",
							/* 318 */ (response.getTicketUrl() != null) ? response.getTicketUrl() : "",
							/* 319 */ (response.getMaskedPAN() != null) ? response.getMaskedPAN() : "");
					/* 320 */ LOGGER.info("RESPUESTA DE ACTUALIZACION DE BD - {}", callTransaccion);
					/* 321 */ LOGGER.info("RESPUESTA AUTORIZACION BANCARIA POR ID - {}", this.GSON.toJson(response));
					/* 322 */ response.setIdTransaccion(transaction.getIdBitacora());
					/* 323 */ LOGGER.info("RESPUESTA DE PAGO MANUAL - {}", this.GSON.toJson(response));
				} else {
					/* 325 */ response.setCode(-50);
					/* 326 */ response.setMessage("NO HA SIDO POSIBLE PROCESAR SU TRANSACCION. TARJETA NO EXISTE.");
				}
			} else {
				/* 329 */ response.setCode(-50);
				/* 330 */ response.setMessage("NO HA SIDO POSIBLE PROCESAR SU TRANSACCION. TRANSACCION NO EXISTE.");
			}
			/* 332 */ } catch (Exception e) {
			/* 333 */ e.printStackTrace();
			/* 334 */ response.setCode(-50);
			/* 335 */ response.setMessage("NO HA SIDO POSIBLE PROCESAR SU TRANSACCION. INTENTELO NUEVAMENTE.");
		}
		/* 337 */ return response;
	}

	public ResponseAuthorization mobilecardPayRequest(Integer idApp, Integer idPais, String idioma,
			MobilecardPayRequest transaction) {
		/* 343 */ ResponseAuthorization response = new ResponseAuthorization();
		try {
			/* 345 */ LOGGER.info("SOLICITANDO LA AUTORIZACION BANCARIA MOBILECARD PAY - {}",
					this.GSON.toJson(transaction));
			/* 346 */ response = this.billPocketClient.mobilecardPay(transaction);
			/* 347 */ LOGGER.info("RESPUESTA DE MOBILECARD PAY - {}", this.GSON.toJson(response));
			/* 348 */ } catch (Exception e) {
			/* 349 */ e.printStackTrace();
			/* 350 */ response = new ResponseAuthorization();
			/* 351 */ response.setCode(-50);
			/* 352 */ response.setMessage("NO HA SIDO POSIBLE PROCESAR SU TRANSACCION. INTENTELO NUEVAMENTE.");
		}
		/* 354 */ return response;
	}

	public ResponseAuthorization authorizationByUserScanRequest(Integer idApp, Integer idPais, String idioma,
			TransactionRequest transaction) {
		/* 360 */ ResponseAuthorization response = new ResponseAuthorization();
		/* 361 */ ResponseEnrollmentCard enrollmentCard = null;

		/* 363 */ Authorization authorization = null;
		/* 364 */ String callTransaccion = null;
		/* 365 */ BaseResponse actualizaTran = null;
		/* 366 */ LOGGER.info("SOLICITANDO LA AUTORIZACION BANCARIA CON BILL POCKET - {}",
				this.GSON.toJson(transaction));
		try {
			/* 368 */ callTransaccion = this.authorizationRepository.insertatransaccion_billpocket(
					String.valueOf(transaction.getIdUsuario()), idApp/* 369 */ .intValue(), transaction.getIdioma(),
					transaction.getIdProveedor(), 9, transaction.getIdProducto(), transaction/* 370 */ .getConcepto(),
					transaction.getReferencia(), transaction.getCargo(), transaction/* 371 */ .getComision(),
					transaction.getIdTarjeta(), transaction.getTipoTarjeta(), transaction/* 372 */ .getImei(),
					transaction.getSoftware(), transaction.getModelo(), transaction.getLat(),
					transaction/* 373 */ .getLon(), null, null, transaction.getEmisor(), transaction.getOperacion());
			/* 374 */ LOGGER.info("RESPUESTA DE DB: {}", callTransaccion);
			/* 375 */ if (callTransaccion != null) {
				/* 376 */ authorization = (Authorization) this.GSON.fromJson(callTransaccion, Authorization.class);
				/* 377 */ if (authorization.getCardToken() != null) {
					/* 378 */ LOGGER.info("TARJETA TOKENIZADA. SE ENVIA A PAGO - {}", this.GSON.toJson(authorization));
					/* 379 */ response = this.billPocketClient.authorizationRequest(authorization,
							transaction.getComision());
					/* 380 */ LOGGER.info("RESPUESTA DE AUTORIZACION - {}", this.GSON.toJson(response));
					/* 381 */ LOGGER.info("ACTUALIZA TRANSACION PARAMS -  ID TRANSACCION: "
							+ authorization/* 382 */ .getIdTransaccion() + " MESSAGE: "
							+ response/* 383 */ .getMessage() + " OPID: " + response/* 384 */ .getOpId()
							+ " TXNISOCODE: " + response/* 385 */ .getTxnISOCode() + " AUTH NUMBER: "
							+ response/* 386 */ .getAuthNumber() + " TICKET: " + response/* 387 */ .getTicketUrl()
							+ " MASKED PAN: " + response/* 388 */ .getMaskedPAN());
					/* 389 */ if (response.getStatus().intValue() != 1) {
						/* 390 */ response.setCode(-1);
					} else {
						/* 392 */ response.setCode(0);
						/* 393 */ response.setStatus(Integer.valueOf(0));
					}
					/* 395 */ callTransaccion = this.authorizationRepository.actualizatransaccion_billpocket(
							authorization/* 396 */ .getIdTransaccion(), response/* 397 */ .getStatus().intValue(),
							response/* 398 */ .getMessage(),
							/* 399 */ (response.getOpId() != null) ? response.getOpId().intValue() : 0L,
							/* 400 */ (response.getTxnISOCode() != null) ? response.getTxnISOCode() : "",
							/* 401 */ (response.getAuthNumber() != null) ? response.getAuthNumber() : "",
							/* 402 */ (response.getTicketUrl() != null) ? response.getTicketUrl() : "",
							/* 403 */ (response.getMaskedPAN() != null) ? response.getMaskedPAN() : "");
					/* 404 */ LOGGER.info("RESPUESTA DE ACTUALIZACION DE BD - {}", callTransaccion);
					/* 405 */ actualizaTran = (BaseResponse) this.GSON.fromJson(callTransaccion, BaseResponse.class);
					/* 406 */ response.setIdTransaccion(authorization.getIdTransaccion());
				}
				/* 408 */ else if (authorization.getCode() == 0) {
					/* 409 */ LOGGER.info("TARJETA NO TOKENIZADA - SOLICITANDO TOKENIZACION - {}",
							this.GSON.toJson(authorization));
					/* 410 */ enrollmentCard = this.billPocketClient.enrollCard(authorization.getPan(),
							authorization.getExpDate(), authorization/* 411 */ .getCvv2(), "");
					/* 412 */ LOGGER.info("RESPUESTA DE TOKENIZACION DE TARJETA - {}",
							this.GSON.toJson(enrollmentCard));
					/* 413 */ if (CODE_SUCESS_BP.intValue() == enrollmentCard.getCode()
							|| enrollmentCard.getCardToken() != null) {
						/* 414 */ LOGGER.info("INSERTANDO TRANSACCION - ID USUARIO - {}",
								Long.valueOf(transaction.getIdUsuario()));
						/* 415 */ callTransaccion = this.authorizationRepository.insertatransaccion_billpocket(
								String.valueOf(transaction.getIdUsuario()), idApp/* 416 */ .intValue(),
								transaction.getIdioma(), transaction.getIdProveedor(), 9, transaction.getIdProducto(),
								transaction/* 417 */ .getConcepto(), transaction.getReferencia(),
								transaction.getCargo(), transaction/* 418 */ .getComision(), transaction.getIdTarjeta(),
								transaction.getTipoTarjeta(), transaction/* 419 */ .getImei(),
								transaction.getSoftware(), transaction.getModelo(), transaction/* 420 */ .getLat(),
								transaction.getLon(), enrollmentCard.getCardToken(),
								enrollmentCard/* 421 */ .getMaskedPan(), transaction.getEmisor(),
								transaction.getOperacion());
						/* 422 */ LOGGER.info("RESPUESTA DE DB: {}", callTransaccion);
						/* 423 */ if (callTransaccion != null) {
							/* 424 */ authorization.setCardToken(enrollmentCard.getCardToken());
							/* 425 */ LOGGER.info("DATOS DE TRANSACCION - {} - CARDTOKEN - {}",
									this.GSON.toJson(transaction), enrollmentCard/* 426 */ .getCardToken()
											+ " MASKED CARD -" + enrollmentCard.getMaskedPan());
							/* 427 */ authorization = (Authorization) this.GSON.fromJson(callTransaccion,
									Authorization.class);
							/* 428 */ response = this.billPocketClient.authorizationRequest(authorization,
									transaction.getComision());
							/* 429 */ LOGGER.info("ACTUALIZA TRANSACION PARAMS -  ID TRANSACCION: "
									+ authorization/* 430 */ .getIdTransaccion() + " MESSAGE: "
									+ response/* 431 */ .getMessage() + " OPID: " + response/* 432 */ .getOpId()
									+ " TXNISOCODE: " + response/* 433 */ .getTxnISOCode() + " AUTH NUMBER: "
									+ response/* 434 */ .getAuthNumber() + " TICKET: "
									+ response/* 435 */ .getTicketUrl() + " MASKED PAN: "
									+ response/* 436 */ .getMaskedPAN());
							/* 437 */ if (response.getStatus().intValue() != 1) {
								/* 438 */ response.setCode(-1);
							} else {
								/* 440 */ response.setCode(0);
								/* 441 */ response.setStatus(Integer.valueOf(0));
							}
							/* 443 */ callTransaccion = this.authorizationRepository.actualizatransaccion_billpocket(
									authorization.getIdTransaccion(), response/* 444 */ .getStatus().intValue(),
									response/* 445 */ .getMessage(),
									/* 446 */ (response.getOpId() != null) ? response.getOpId().intValue() : 0L,
									/* 447 */ (response.getTxnISOCode() != null) ? response.getTxnISOCode() : "",
									/* 448 */ (response.getAuthNumber() != null) ? response.getAuthNumber() : "",
									/* 449 */ (response.getTicketUrl() != null) ? response.getTicketUrl() : "",
									/* 450 */ (response.getMaskedPAN() != null) ? response.getMaskedPAN() : "");
							/* 451 */ LOGGER.info("RESPUESTA DE ACTUALIZACION DE BD - {}", callTransaccion);
							/* 452 */ actualizaTran = (BaseResponse) this.GSON.fromJson(callTransaccion,
									BaseResponse.class);
							/* 453 */ response.setIdTransaccion(authorization.getIdTransaccion());
						} else {
							/* 455 */ response.setCode(-50);
							/* 456 */ response.setMessage("ERROR AL GUARDAR LA TRANSACION EN BD");
						}
					} else {
						/* 459 */ response.setCode(enrollmentCard.getCode());
						/* 460 */ response.setMessage(enrollmentCard.getMessage());
					}
				} else {
					/* 463 */ response.setCode(authorization.getCode());
					/* 464 */ response.setMessage(authorization.getMessage());
				}

				/* 467 */ LOGGER.info("RESPUESTA DE BILL POCKET . {}", this.GSON.toJson(response));
			} else {
				/* 469 */ response = new ResponseAuthorization();
				/* 470 */ response.setCode(-50);
				/* 471 */ response.setMessage("ERROR AL GUARDAR LA TRANSACION EN BD");
			}
			/* 473 */ } catch (Exception e) {
			/* 474 */ e.printStackTrace();
			/* 475 */ response = new ResponseAuthorization();
			/* 476 */ response.setCode(-50);
			/* 477 */ response.setMessage("ERROR AL GUARDAR LA TRANSACION EN BD");
		}
		/* 479 */ return response;
	}

	public ResponseAuthorization threeDResponse(String checkout, String externalId, String total, String authorization,
			String ticketUrl, String opId, String maskedPAN, String message, String token, String signature) {
		/* 486 */ ResponseAuthorization response = new ResponseAuthorization();
		/* 487 */ String callTransaccion = null;
		/* 488 */ BaseResponse actualizaTran = null;
		/* 489 */ Optional<Transaction> transaction = null;
		try {
			/* 491 */ LOGGER.info("ACTUALIZA TRANSACION PARAMS -  CHECKOUT: " + checkout + " ID TRANSACCION: "
					+ externalId + " TOTAL: " + total + " AUTH NUMBER: " + authorization + " TICKET: " + ticketUrl
					+ " OPID: " + opId + " MASKED PAN: " + response

							/* 498 */ .getMaskedPAN()
					+ " MESSAGE: " + message + " TOKEN: " + token + " SIGNATURE: " + signature);

			/* 502 */ callTransaccion = this.authorizationRepository.actualizatransaccion_billpocket(
					Long.valueOf(externalId).longValue(), 1, message,

					/* 505 */ (Long.valueOf(opId) != null) ? Long.valueOf(opId).longValue() : 0L, "",
					(authorization != null) ? authorization : "", (ticketUrl != null) ? ticketUrl : "",
					(maskedPAN != null) ? maskedPAN : "");

			/* 510 */ actualizaTran = (BaseResponse) this.GSON.fromJson(callTransaccion, BaseResponse.class);

			/* 512 */ transaction = this.transactionRepository.findByIdBitacora(Long.valueOf(externalId).longValue());
			/* 513 */ response.setReferencia(((Transaction) transaction.get()).getReferencia());
			/* 514 */ response.setIdTransaccion(Long.valueOf(externalId).longValue());
			/* 515 */ response.setMaskedPAN(maskedPAN);
			/* 516 */ response.setAmount(Double.valueOf(total).doubleValue());
			/* 517 */ response.setAuthNumber(authorization);
			/* 518 */ response.setDateTime((new Date()).getTime());
			/* 519 */ response.setMessage(message);
			/* 520 */ response.setOpId(Integer.valueOf(opId));
		}
		/* 522 */ catch (Exception e) {
			/* 523 */ e.printStackTrace();
			/* 524 */ response = new ResponseAuthorization();
			/* 525 */ response.setCode(-50);
			/* 526 */ response.setMessage("ERROR AL GUARDAR LA TRANSACION EN BD");
		}
		/* 528 */ LOGGER.info("RESPUESTA DE 3D SECURE - {}", this.GSON.toJson(response));
		/* 529 */ return response;
	}

	public String cardTokenValidation(Integer idTarjeta) {
		/* 534 */ return null;
	}

	public ResponseAuthorization refund(Integer idApp, Integer idPais, String idioma, long idTransaccion) {
		/* 539 */ ResponseAuthorization response = new ResponseAuthorization();
		/* 540 */ String respBD = null;
		/* 541 */ Refund validate = null;
		try {
			/* 543 */ LOGGER.info("SELECT reversatransaccion_billpocket(" + idApp + ", 9, " + idTransaccion + ")");
			/* 544 */ respBD = this.authorizationRepository.reversatransaccion_billpocket(idApp.intValue(), 9L,
					idTransaccion);
			/* 545 */ LOGGER.info("RESPUESTA DE REVERSA TRANSACCION DB - {}", respBD);
			/* 546 */ if (respBD != null) {
				/* 547 */ validate = (Refund) this.GSON.fromJson(respBD, Refund.class);
				/* 548 */ if (validate.getCode() == 0) {
					/* 549 */ response = this.billPocketClient.refundRequest(Integer.valueOf(validate.getOpId()));
					/* 550 */ LOGGER.info("RESPUESTA DE BILL POCKET - {}", this.GSON.toJson(response));
					/* 551 */ respBD = this.authorizationRepository.actualizareverso_billpocket(idTransaccion,
							response.getStatus().intValue(), response/* 552 */ .getMessage(),
							/* 553 */ (response.getOpId() != null) ? response.getOpId().intValue() : validate.getOpId(),
							response/* 554 */ .getTxnISOCode(),
							/* 555 */ (response.getAuthNumber() != null) ? response.getAuthNumber() : "",
							/* 556 */ (response.getMaskedPAN() != null) ? response.getMaskedPAN() : "");
					/* 557 */ LOGGER.info("RESPUESTA DE ACTUALIZA REVERSA DB - {}", respBD);
				} else {
					/* 559 */ response = new ResponseAuthorization();
					/* 560 */ response.setCode(validate.getCode());
					/* 561 */ response.setMessage(validate.getMessage());
				}
			} else {
				/* 564 */ response = new ResponseAuthorization();
				/* 565 */ response.setCode(-5000);
				/* 566 */ response.setMessage("ERROR EN BD");
			}
			/* 568 */ } catch (Exception e) {
			/* 569 */ e.printStackTrace();
		}
		/* 571 */ return response;
	}

	public ResponseAuthorization checkTransaction(Integer idApp, Integer idPais, String idioma, Integer opId) {
		/* 576 */ ResponseAuthorization response = new ResponseAuthorization();
		try {
			/* 578 */ response = this.billPocketClient.checkTransaction(opId);
			/* 579 */ } catch (Exception e) {
			/* 580 */ e.printStackTrace();
		}

		/* 583 */ return response;
	}
}