package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.client.PushRequest;
import com.addcel.bill.pocket.BillPocket.client.PushResponse;

public interface PushClient {
	PushResponse sendNotification(PushRequest paramPushRequest);
}