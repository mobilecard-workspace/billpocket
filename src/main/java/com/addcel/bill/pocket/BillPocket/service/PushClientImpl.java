package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.client.PushRequest;
import com.addcel.bill.pocket.BillPocket.client.PushResponse;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class PushClientImpl implements PushClient {
	private static final String PATH_PUSH = "http://localhost/PushNotifications/sendMessage";
	/* 23 */ Gson GSON = new Gson();
	private static final String AUTH = "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx";
	@Autowired
	RestTemplate restTemplate;
	/* 25 */ private static final Logger LOG = LoggerFactory.getLogger(PushClient.class);

	public PushResponse sendNotification(PushRequest request) {
		/* 29 */ PushResponse response = null;
		/* 30 */ LOG.debug("Enviando notificacion push");
		try {
			/* 32 */ HttpHeaders headers = new HttpHeaders();
			/* 33 */ headers.add("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
			/* 34 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 35 */ HttpEntity<PushRequest> requestEntity = new HttpEntity(request, (MultiValueMap) headers);
			/* 36 */ ResponseEntity<String> respPush = this.restTemplate.exchange(
					"http://localhost/PushNotifications/sendMessage", HttpMethod.POST, requestEntity, String.class,
					new Object[0]);

			/* 38 */ LOG.debug("Respuesta del servicio push: " + respPush);
			/* 39 */ response = (PushResponse) this.GSON.fromJson((String) respPush.getBody(), PushResponse.class);
			/* 40 */ } catch (Exception e) {
			/* 41 */ LOG.error("Error al enviar la notificacion al usuario: " + e.getMessage());
		}
		/* 43 */ return response;
	}
}