package com.addcel.bill.pocket.BillPocket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.bill.pocket.BillPocket.bean.ScanAndPayRequest;
import com.addcel.bill.pocket.BillPocket.bean.TransfersResponse;
import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import com.google.gson.Gson;

@Service
public class SPServiceImpl implements SPService {
	/* 18 */ private static final Logger LOGGER = LoggerFactory.getLogger(BillPocketClientServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	/* 23 */ private Gson GSON = new Gson();

	public TransfersResponse processPayment(Transaction transaction) {
		/* 27 */ ScanAndPayRequest request = new ScanAndPayRequest();
		/* 28 */ TransfersResponse response = new TransfersResponse();
		try {
			/* 30 */ request.setIdEstablecimiento(transaction.getIdEstablecimiento().longValue());
			/* 31 */ request.setComision(transaction.getComision());
			/* 32 */ request.setConcepto(transaction.getConcepto());
			/* 33 */ request.setEmailCliente(transaction.getDestino());
			/* 34 */ request.setIdBitacora(transaction.getIdBitacora().longValue());
			/* 35 */ request.setMonto(transaction.getCargo());
			/* 36 */ request.setPropina(Double.valueOf(0.0D));
			/* 37 */ request.setStatus(1);

			/* 39 */ LOGGER.info("ENVIANDO PETICION A SCAN & PAY - ID BITACORA - {}", transaction.getIdBitacora());
			/* 40 */ HttpHeaders headers = new HttpHeaders();
			/* 41 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 42 */ HttpEntity<ScanAndPayRequest> requestH2h = new HttpEntity(request, (MultiValueMap) headers);
			/* 43 */ LOGGER.info("DATOS ENVIADOS A SCAN & PAY - {}", this.GSON.toJson(requestH2h));
			/* 44 */ ResponseEntity<TransfersResponse> respEnrollCard = this.restTemplate.exchange(
					"http://localhost/CuantoTeDebo/1/1/es/pago3DSBP", HttpMethod.POST, requestH2h,
					TransfersResponse.class, new Object[0]);

			/* 46 */ LOGGER.info("RESPUESTA DE RESTEMPLATE - {}", this.GSON.toJson(respEnrollCard));
			/* 47 */ response = (TransfersResponse) respEnrollCard.getBody();
			/* 48 */ LOGGER.info("RESPUESTA DE SCAN & PAY - {}", this.GSON.toJson(response));
			/* 49 */ if (0 == response.getStatus().intValue()) {
				/* 50 */ response.setCode(Integer.valueOf(0));
			} else {
				/* 52 */ response.setCode(Integer.valueOf(-100));
				/* 53 */ response.setMessage("Ocurrio un error al contactar al proveedor. ");
			}
			/* 55 */ } catch (Exception e) {
			/* 56 */ e.printStackTrace();
			/* 57 */ response.setCode(Integer.valueOf(-100));
			/* 58 */ response.setMessage("No fue posible realizar la transferencia, error al contactar al proveedor.");
		}
		/* 60 */ return response;
	}
}