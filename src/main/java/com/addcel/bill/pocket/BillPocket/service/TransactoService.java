package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.TransactoResponse;
import com.addcel.bill.pocket.BillPocket.client.PushParams;
import com.addcel.bill.pocket.BillPocket.client.PushRequest;
import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import com.addcel.bill.pocket.BillPocket.model.domain.Usuario;
import com.addcel.bill.pocket.BillPocket.model.repository.TransactionRepository;
import com.addcel.bill.pocket.BillPocket.model.repository.UsuarioRepository;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactoService {
	/* 22 */ private static final Logger LOGGER = LoggerFactory.getLogger(TransactoService.class);

	@Autowired
	private AntadClient antadClient;

	@Autowired
	private PushClient pushClient;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private TransactionRepository repository;

	public boolean processAntadPayment(long idBitacora) {
		/* 37 */ TransactoResponse transacto = null;
		/* 38 */ PushRequest pushReq = null;
		/* 39 */ Optional<Usuario> usuario = null;
		/* 40 */ Optional<Transaction> transaction = null;
		try {
			/* 42 */ LOGGER.info("BUSCANDO TRANSACCION....");
			/* 43 */ transaction = this.repository.findByIdBitacora(idBitacora);
			/* 44 */ if (transaction.isPresent()) {
				/* 45 */ LOGGER.info("TRANSACCION ENCONTRADA EXITOSAMENTE - {} - BUSCANDO USUARIO - {}",
						Long.valueOf(idBitacora), ((Transaction) transaction.get()).getIdUsuario());
				/* 46 */ usuario = this.usuarioRepository
						.findByIdUsuario(((Transaction) transaction.get()).getIdUsuario());
				/* 47 */ if (usuario.isPresent()) {
					/* 48 */ LOGGER.info("USUARIO ENCONTRADO EXITOSAMENTE - {}", ((Usuario) usuario.get()).getLogin());
					/* 49 */ transacto = this.antadClient.aprovisionaServicio(idBitacora);
					/* 50 */ if (transacto.getIdError() == 0) {
						/* 51 */ LOGGER.info("SERVICIO APROVISIONADO EXITOSAMENTE - LOGIN - {} USUARIOS - {}",
								((Usuario) usuario.get()).getLogin(), Long.valueOf(idBitacora));
						/* 52 */ this.pushClient.sendNotification(pushData(((Usuario) usuario.get()).getIdUsuario(),
								((Usuario) usuario/* 53 */ .get()).getNombre() + ""
										+ ((Usuario) usuario.get()).getApellidoP(),
								((Transaction) transaction/* 54 */ .get()).getCargo().doubleValue(),
								((Transaction) transaction.get()).getConcepto()));
						/* 55 */ return true;
					}
				}
			}
			/* 59 */ } catch (Exception e) {
			/* 60 */ e.printStackTrace();
		}
		/* 62 */ return false;
	}

	private PushRequest pushData(long idUsuario, String nombres, double monto, String concepto) {
		/* 67 */ PushRequest pushReq = new PushRequest();
		try {
			/* 69 */ pushReq.setId_usuario(idUsuario);
			/* 70 */ pushReq.setIdApp(1);
			/* 71 */ pushReq.setIdioma("es");
			/* 72 */ pushReq.setIdPais(1);
			/* 73 */ pushReq.setModulo(3L);
			/* 74 */ pushReq.setTipoUsuario("USUARIO");
			/* 75 */ List<PushParams> paramsList = new LinkedList<>();
			/* 76 */ PushParams params = new PushParams();
			/* 77 */ params.setName("nombre");
			/* 78 */ params.setValue(nombres);
			/* 79 */ paramsList.add(params);
			/* 80 */ params.setName("monto");
			/* 81 */ params.setValue(monto + "");
			/* 82 */ paramsList.add(params);
			/* 83 */ params.setName("servicio");
			/* 84 */ params.setValue(concepto);
			/* 85 */ paramsList.add(params);
			/* 86 */ } catch (Exception e) {
			/* 87 */ e.printStackTrace();
		}
		/* 89 */ return pushReq;
	}
}