package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.TransfersResponse;
import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;

public interface TransfersService {
	TransfersResponse doTransferSTP(Transaction paramTransaction);
}