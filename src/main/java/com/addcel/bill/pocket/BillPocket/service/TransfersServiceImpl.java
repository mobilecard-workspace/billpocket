package com.addcel.bill.pocket.BillPocket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.bill.pocket.BillPocket.bean.TransferRequest;
import com.addcel.bill.pocket.BillPocket.bean.TransfersResponse;
import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import com.google.gson.Gson;

@Service
public class TransfersServiceImpl implements TransfersService {
	/* 16 */ private static final Logger LOGGER = LoggerFactory.getLogger(BillPocketClientServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	/* 21 */ private Gson GSON = new Gson();

	public TransfersResponse doTransferSTP(Transaction transaction) {
		/* 25 */ TransferRequest request = new TransferRequest();
		/* 26 */ TransfersResponse response = new TransfersResponse();
		try {
			/* 28 */ request.setIdBitacora(transaction.getIdBitacora().longValue());
			/* 29 */ request.setIdUser(transaction.getIdUsuario().longValue());
			/* 30 */ request.setIdCard(transaction.getIdTarjeta().intValue());
			/* 31 */ request.setAccountId(transaction.getReferencia());
			/* 32 */ request.setConcept(transaction.getConcepto());
			/* 33 */ request.setComision(transaction.getComision().doubleValue());
			/* 34 */ request.setImei(transaction.getImei());
			/* 35 */ request.setSoftware(transaction.getSoftware());
			/* 36 */ request.setModel(transaction.getModel());
			/* 37 */ request.setLat(transaction.getCy().doubleValue());
			/* 38 */ request.setLon(transaction.getCx().doubleValue());
			/* 39 */ request.setReferencia(transaction.getReferencia());
			/* 40 */ request.setEmisor(String.valueOf(transaction.getIdProducto()));
			/* 41 */ request.setOperation("");
			/* 42 */ request.setStatus(1);
			/* 43 */ request.setAmount(transaction.getCargo().doubleValue());
			/* 44 */ LOGGER.info("ENVIANDO PETICION A H2H ID BITACORA - {}", transaction.getIdBitacora());
			/* 45 */ HttpHeaders headers = new HttpHeaders();
			/* 46 */ headers.setContentType(MediaType.APPLICATION_JSON);
			/* 47 */ HttpEntity<TransferRequest> requestH2h = new HttpEntity(request, (MultiValueMap) headers);
			/* 48 */ ResponseEntity<TransfersResponse> respEnrollCard = this.restTemplate.exchange(
					"http://localhost/H2HPayment/1/1/es/pago3dsBP", HttpMethod.POST, requestH2h,
					TransfersResponse.class, new Object[0]);

			/* 50 */ LOGGER.info("RESPUESTA DE RESTEMPLATE - {}", this.GSON.toJson(respEnrollCard));
			/* 51 */ response = (TransfersResponse) respEnrollCard.getBody();
			/* 52 */ LOGGER.info("RESPUESTA DE H2H - {}", this.GSON.toJson(response));
			/* 53 */ if (0 == response.getStatus().intValue()) {
				/* 54 */ response.setCode(Integer.valueOf(0));
			} else {
				/* 56 */ response.setCode(Integer.valueOf(-100));
				/* 57 */ response.setMessage("Ocurrio un error al contactar al proveedor. ");
			}
			/* 59 */ } catch (Exception e) {
			/* 60 */ e.printStackTrace();
			/* 61 */ response.setCode(Integer.valueOf(-100));
			/* 62 */ response.setMessage("No fue posible realizar la transferencia, error al contactar al proveedor.");
		}
		/* 64 */ return response;
	}
}