package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.Webhook;

public interface WebHookService {
	boolean processAntadPayment(Webhook paramWebhook);
}