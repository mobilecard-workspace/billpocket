package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.BaseResponse;
import com.addcel.bill.pocket.BillPocket.bean.TransactoResponse;
import com.addcel.bill.pocket.BillPocket.bean.TransfersResponse;
import com.addcel.bill.pocket.BillPocket.bean.Webhook;
import com.addcel.bill.pocket.BillPocket.client.PushRequest;
import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import com.addcel.bill.pocket.BillPocket.model.domain.Usuario;
import com.addcel.bill.pocket.BillPocket.model.repository.AuthorizationRepository;
import com.addcel.bill.pocket.BillPocket.model.repository.TransactionRepository;
import com.addcel.bill.pocket.BillPocket.model.repository.UsuarioRepository;
import com.addcel.bill.pocket.BillPocket.utils.Constants;
import com.google.gson.Gson;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebHookServiceImpl implements WebHookService {
	/* 30 */ private static final Logger LOGGER = LoggerFactory.getLogger(WebHookServiceImpl.class);

	@Autowired
	private AntadClient antadClient;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private TransactionRepository repository;

	@Autowired
	private TransfersService transfersService;

	@Autowired
	private SPService spService;

	@Autowired
	private AuthorizationRepository authorizationRepository;

	/* 50 */ private Gson GSON = new Gson();

	public boolean processAntadPayment(Webhook webhook) {
		/* 53 */ TransactoResponse transacto = null;
		/* 54 */ TransfersResponse transfersResponse = null;
		/* 55 */ PushRequest pushReq = null;
		/* 56 */ Optional<Usuario> usuario = null;
		/* 57 */ Optional<Transaction> transaction = null;
		/* 58 */ BaseResponse actualizaTran = null;
		/* 59 */ String callTransaccion = null;
		try {
			/* 61 */ LOGGER.info("BUSCANDO TRANSACCION - ID TRANSACCION - {}", webhook.getExternalId());
			/* 62 */ transaction = this.repository.findByIdBitacora(Long.valueOf(webhook.getExternalId()).longValue());
			/* 63 */ if (transaction.isPresent()) {
				/* 64 */ if (Constants.PROVEEDOR_SP == ((Transaction) transaction.get()).getIdProveedor())
				/* 65 */ {
					LOGGER.info("TRANSACCION SCAN & PAY ENCONTRADA - {} - ID TRANSACCION - {}",
							webhook.getExternalId());
					/* 66 */ transfersResponse = this.spService.processPayment(transaction.get());
				}
				/* 67 */ else {
					if (Constants.PROVEEDOR_TRANSFERENCIAS == ((Transaction) transaction.get()).getIdProveedor()) {
						/* 68 */ LOGGER.info("TRANSACCION TRANSFERENCIAS ENCONTRADA - {} - BUSCANDO USUARIO - {}",
								webhook.getExternalId(), ((Transaction) transaction.get()).getIdUsuario());
						/* 69 */ transfersResponse = this.transfersService.doTransferSTP(transaction.get());
						/* 70 */ return true;
					}
					/* 72 */ LOGGER.info("TRANSACCION ANTAD ENCONTRADA - {} - BUSCANDO USUARIO - {}",
							webhook.getExternalId(), ((Transaction) transaction.get()).getIdUsuario());
					/* 73 */ usuario = this.usuarioRepository
							.findByIdUsuario(((Transaction) transaction.get()).getIdUsuario());
					/* 74 */ if (usuario.isPresent()) {
						/* 75 */ LOGGER.info("USUARIO ENCONTRADO EXITOSAMENTE - {}",
								((Usuario) usuario.get()).getLogin());
						/* 76 */ transacto = this.antadClient
								.aprovisionaServicio(Long.valueOf(webhook.getExternalId()).longValue());
						/* 77 */ if (transacto.getIdError() == 0) {
							/* 78 */ LOGGER.info("SERVICIO APROVISIONADO EXITOSAMENTE - LOGIN - {} USUARIOS - {}",
									((Usuario) usuario.get()).getLogin(), webhook.getExternalId());
							/* 79 */ return true;
						}
					}
				}

			}
			/* 84 */ LOGGER.info("ACTUALIZA TRANSACION PARAMS -  CHECKOUT: " + webhook/* 85 */ .getCheckout()
					+ " ID TRANSACCION: " + webhook/* 86 */ .getExternalId() + " TOTAL: " + webhook/* 87 */ .getTotal()
					+ " AUTH NUMBER: " + webhook/* 88 */ .getAuthorization() + " TICKET: "
					+ webhook/* 89 */ .getTicketUrl() + " OPID: " + webhook/* 90 */ .getOpId() + " MASKED PAN: "
					+ webhook/* 91 */ .getMaskedPAN() + " MESSAGE: " + webhook/* 92 */ .getMessage() + " TOKEN: "
					+ webhook/* 93 */ .getToken() + " SIGNATURE: " + webhook/* 94 */ .getSignature());
			/* 95 */ callTransaccion = this.authorizationRepository.actualizatransaccion_billpocket(
					Long.valueOf(Long.valueOf(webhook.getExternalId()).longValue()).longValue(), 1, webhook

							/* 97 */ .getMessage(),
					/* 98 */ (Long.valueOf(webhook.getOpId()) != null) ? Long.valueOf(webhook.getOpId()).longValue()
							: 0L,
					"",

					/* 100 */ (webhook.getAuthorization() != null) ? webhook.getAuthorization() : "",
					/* 101 */ (webhook.getTicketUrl() != null) ? webhook.getTicketUrl() : "",
					/* 102 */ (webhook.getMaskedPAN() != null) ? webhook.getMaskedPAN() : "");
			/* 103 */ actualizaTran = (BaseResponse) this.GSON.fromJson(callTransaccion, BaseResponse.class);
			/* 104 */ LOGGER.info("RESPUESTA DE ACTUALIZACION TRANSACCION: {}", this.GSON.toJson(actualizaTran));
			/* 105 */ } catch (Exception e) {
			/* 106 */ e.printStackTrace();
		}
		/* 108 */ return false;
	}
}